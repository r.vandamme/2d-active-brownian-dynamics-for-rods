## Introduction ##

This code can be used to simulate the dynamics of self-propelled rodlike particles. It was used to do so for DOI:10.1063/1.5086733. 

## Building (Linux, using Cmake) ##

Building the code requires two external libraries: the [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) vector math library and the [Voro++](http://math.lbl.gov/voro++/) 3D Voronoi construction library. Eigen is header-only, but Voro++ needs to be compiled. Once these are obtained, compile this code by creating a directory `bin`, go there and build with `cmake ..` followed by `make`.

## Usage ##

Once the code is built, you should have an executable called `bd`. It accepts the following input parameters:

`-Npart X` sets the number of particles to X.

`-Packfrac X` sets the packing fraction to X.

`-AspectRatio X` sets the rod aspect ratio to X. Set to 1 for disks.

`-Swimspeed X` sets the swimspeed to X. For the chosen nondimensionalization, this swimspeed is equal to the Peclet number.

`-Totaltime X` sets the total time to simulate to X, in units of 1/D_r.

A valid simulation with 100 rods of aspect ratio 2.0 at Pe=50 and packing fraction 0.5 would thus be: `./bd -Npart 100 -Packfrac 0.5 -Swimspeed 50 -AspectRatio 2`.