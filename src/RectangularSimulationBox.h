#ifndef BD_SIMBOX_H
#define BD_SIMBOX_H

#include <iostream>
#include <eigen3/Eigen/Geometry>

using Eigen::Vector2d;

class SimulationBox{
private:
	Vector2d box_length_;
	Vector2d box_half_length_;
	// TODO Check whether storing these can speed things up
	Vector2d box_length_inverse_;
	Vector2d box_half_length_inverse_;
	double area_;
public:
	SimulationBox(void);
	SimulationBox(double x);
	SimulationBox(double x, double y);
	SimulationBox(const Vector2d& vec);
	~SimulationBox(void){};

	// Setters
  void set(const Vector2d& vec);
	void set(double x);
	void set(double x, double y);

	// Getters
	const Vector2d& L(void)const{ return box_length_; }
  const Vector2d& hL(void)const{ return box_half_length_; }
	const Vector2d& iL(void)const{ return box_length_inverse_; }
  const Vector2d& ihL(void)const{ return box_half_length_inverse_; }
	double area(void)const{ return area_; }

	// Periodic distance functions
	Vector2d nearest_image_vector(const Vector2d&, const Vector2d&)const;
	double nearest_image_distancesq(const Vector2d&, const Vector2d&)const;
	double nearest_image_distance(const Vector2d&, const Vector2d&)const;
	Vector2d nearest_image_pos(const Vector2d&, const Vector2d&)const;

	const void print(void)const{
		std::cout << "box:\n";
		std::cout << box_length_ << '\n';
	}
};

// Setters
inline void SimulationBox::set(const Vector2d& vec){
	box_length_              = vec;
  box_half_length_         = 0.5 * box_length_.array();
	box_length_inverse_      = 1.0 / box_length_.array();
  box_half_length_inverse_ = 1.0 / box_half_length_.array();
	area_               = box_length_[0] * box_length_[1];
}
inline void SimulationBox::set(double x){
	Vector2d vec(x,x);
	set(vec);
}
inline void SimulationBox::set(double x, double y){
	Vector2d vec(x,y);
	set(vec);
}

// Constructors
inline SimulationBox::SimulationBox(void){
  Vector2d vec(1,1);
	set(vec);
}
inline SimulationBox::SimulationBox(double x){
	set(x);
}
inline SimulationBox::SimulationBox(double x, double y){
	set(x,y);
}
inline SimulationBox::SimulationBox(const Vector2d& vec){
	set(vec);
}


// Periodic distance functions

// Nearest image vector
inline Vector2d SimulationBox::nearest_image_vector(const Vector2d& pos1, const Vector2d& pos2)const{
	// Check each cardinal direction for distance > box length, then correct nearvec accordingly
	Vector2d nearvec = pos2 - pos1;
  for(size_t d = 0; d < 2; d++){
    if(fabs(nearvec[d]) > box_half_length_[d]){
      if(nearvec[d] > 0.0) nearvec[d] -= box_length_[d];
      else                 nearvec[d] += box_length_[d];
    }
  }
	// Return what is now the nearest image vector from pos1 to pos2
	return nearvec;
}
// Nearest image distance squared
inline double SimulationBox::nearest_image_distancesq(const Vector2d& pos1, const Vector2d& pos2)const{
	// Call nearest_image_vector and return its squared norm
	return nearest_image_vector(pos1, pos2).squaredNorm();
}
// Nearest image distance
inline double SimulationBox::nearest_image_distance(const Vector2d& pos1, const Vector2d& pos2)const{
	// Call nearest_image_distancesq and return its square root
	return sqrt(nearest_image_distancesq(pos1, pos2));
}

#endif
