#ifndef MC_CLASS_STATISTICS_H
#define MC_CLASS_STATISTICS_H

#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

/* Namespace that contains classes and functions to measure stuff and save it */
namespace statistics{


	class Observable{ // Class that describes a single observable scalar value e.g. density
	private:
		unsigned long int samples_ = 0; // Number of times this value has been measured
		double current_   = 0.0;        // Latest measured value of observable
		double mean_      = 0.0;        // Current mean of all measured values
		double mvariance_ = 0.0;        // Current variance times number of measurements
	public:
		// Constructors / destructors
		Observable() : samples_(0), mean_(0.0), mvariance_(0.0) {};
		~Observable(){};
		// Functions
		void clear(void);
		void add_measurement(double value);
		// Getters
		double mean(void)const     { return mean_; }
		double stdev(void)const    { return sqrt(variance()); }
		double current(void)const  { return current_; }
		double variance(void)const { return ( (samples_!=0) ? (mvariance_ / samples_) : 0.0 ); }
		double stdevmean(void)const{ return stdev() / sqrt(samples_); }
		// Save to a file
		void save_current_append_to_file(const std::string filename, double time);
		void save_mean_to_file(const std::string filename);
	};



	class Histogram{ // Histogram with a fixed size and known domain
	private:
		bool initialized_ = false;
		double startx_, endx_;
		double binwidth_;
		std::vector<Observable> values_;
	public:
		// Declaration constructor
		Histogram(void){};
		// Constructors with known size
		Histogram(size_t n_bins, double startx, double endx);
		Histogram(double binwidth, double startx, double endx);
		// Destructor
		// virtual ~Histogram(void);
		// Functions
		size_t size(void)const{return values_.size(); }
		double startx(void)const{ return startx_; }
		double endx(void)const{ return endx_; }
		double mean_at(size_t bin)const{ return values_[bin].mean(); }
		size_t bin_of(double xvalue)const{ return (xvalue - startx_) / binwidth_; }
		double xvalue_of(size_t bin)const{ return bin * binwidth_ + startx_; }
		double binwidth(void)const{ return binwidth_; }
		bool is_initialized(void)const{ return initialized_; }
		void clear(void);
		void initialize_with_size(size_t n_bins, double binwidth);
		void initialize_with_size(size_t n_bins, double startx, double endx);
		void initialize_with_size(double binwidth, double startx, double endx);
		void clear_bin(size_t bin){ values_[bin].clear(); }
		void add_measurement(size_t bin, double yvalue); // NOTE: should these be explicit?
		void add_measurement(double xvalue, double yvalue);
		void save_to_file(std::string file_name)const;
	};



	class DynamicHistogram{ // Histogram without a fixed size that is grown dynamically
	private:
		bool initialized_ = false;
		double startx_;
		double binwidth_;
		std::vector<Observable> values_;
	public:
		// Declaration constructor
		DynamicHistogram(void){};
		// Constructors with known size
		DynamicHistogram(size_t n_bins, double binwidth, double startx);
		// Destructor
		// virtual ~Histogram(void);
		// Functions
		void reserve(size_t size){ values_.reserve(size); }
		size_t size(void)const{return values_.size(); }
		double binwidth(void)const{ return binwidth_; }
		double startx(void)const{ return startx_; }
		double endx(void)const{ return values_.size() * binwidth_; }
		double mean_at(size_t bin)const{ return values_[bin].mean(); }
		size_t bin_of(double xvalue)const;
		double xvalue_of(size_t bin)const;
		bool initialized(void)const{ return initialized_; }
		void clear(void);
		void initialize(size_t n_bins, double binwidth, double startx);
		void clear_bin(size_t bin){ values_[bin].clear(); }
		void add_measurement(size_t bin, double yvalue); // NOTE: should these be explicit?
		void add_measurement(double xvalue, double yvalue);
		void grow_to_size(size_t new_n_bins);
		void grow_to_include(double xvalue);
		void save_to_file(std::string file_name)const;
	};




	// class ScalarField{ // Class that describes a spatial field of scalar values e.g. temperature field
	// };

	// class VectorField{ // Class that describes a spatial field of vector values e.g. flow field
	// };
}
























#endif
