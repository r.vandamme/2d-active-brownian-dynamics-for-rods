#ifndef BD_CLASS_CELL_H
#define BD_CLASS_CELL_H

#include <list>
#include <vector>
#include <eigen3/Eigen/Geometry>
#include "Particle.h"
#include "RectangularSimulationBox.h"


// neighbour list: for every particle we need a list of nearby other particles

// we make this list by first constructing a cell list where we sort all particles into cells,
// then for every particle we thread over all particles in nearby cells to see if they are close
// enough to be considered a neighbour

// we also have to recreate the neighbour list every so often. To know when to do this, we save the
// positions of all particles when we make the list, then keep track of the two largest
// displacements from these positions. If there are two particles that have been displaced further
// than the criterion (so r1 + r2 > rc), we recreate the list by rebuilding the cell list and from
// there rebuilding the neighbour list.

// Edge cases to keep track of:
// - If the box is so small that the neighbour cutoff is larger than half the box size we will run
// into trouble when calculating the displacements from the current positions.

using Eigen::Vector2d;
using Eigen::Matrix2d;


class Cell{
private:
	size_t index_;                       // Index of this cell in the cell array
	std::vector<size_t> particle_indices;   // Indices of particles in this cell
	std::vector<size_t> neighbour_cells;    // Indices of neighbouring cells
public:
	// Constructor. Reserves some memory for optimization
	Cell(size_t i){ index_ = i; neighbour_cells.reserve(9); /*particle_indices.reserve(20);*/ }

	// Get index
	size_t index(void)const{ return index_; }

	// Add a single particle
	void add_particle(size_t i){ particle_indices.push_back(i); }
	// Clear all particles
	void clear_particles(void){ particle_indices.clear(); }
	// Number of particles
	size_t n_particles(void){ return particle_indices.size(); }
	// Get vector of all particles in this cell
	const std::vector<size_t>& particles()const{ return particle_indices; }


  // Adds index of the neighbouring cell
  void add_neighbour(size_t i){ neighbour_cells.push_back(i); }
	// Clear all neighbours
	void clear_neighbours(void){ neighbour_cells.clear(); }
	// Number of neighbours
	size_t n_neighbours(void)const{ return neighbour_cells.size(); }
	// Checks whether index i is already in the neighbour list
	bool contains_neighbour(size_t i)const{
		for(size_t j = 0; j < neighbour_cells.size(); j++){
			if(i == neighbour_cells[j]){ return true; }
		}
		return false;
	}
  // Returns list of all indices of cell neighbours
  const std::vector<size_t>& neighbours()const{ return neighbour_cells; }


	// Prints some info about this cell for debugging
	void diagnostic(void)const{
		printf("index %lu\n",index_);
		printf("size cap particle vector: %d %d\n",static_cast<int>(particle_indices.size()),static_cast<int>(particle_indices.capacity()) );
		printf("size cap cell vector: %d %d\n\n",static_cast<int>(neighbour_cells.size()),static_cast<int>(neighbour_cells.capacity()) );
	}
};



class CellList{
private:
	// Pointers to a simulation box and a vector of particles in this box
	SimulationBox& box_;
	std::vector<Particle>& particles_;
	// Store the box lengths for which the cell list was made
	Eigen::Vector2d old_box_L_;
	// Cell neighbour list, a list of cells that hold particle indices
	std::vector<Cell> cells_;
	// Cells along each direction
	size_t nx_,ny_;
	// Particle cell list. The complement to the cell list, this holds the cell id of every particle
	std::vector<size_t> particle_cells_;
	// Cutoff that defines the minimum cell size
	double cutoff_;
	// Current size of the containers: used to track changes in total number of particles
	size_t current_container_size_;

	size_t coords_to_cell(const Vector2d&);
	void find_size_and_create_cells(double);
	void find_cell_neighbours(void);
	void sort_particles(void);
	void resize_containers(size_t n_elements);
	Cell& cell(int i){ return cells_[i]; }
public:
	CellList(SimulationBox&, std::vector<Particle>&, double);
	void build(double);
	void update(double);
	size_t particles_cell_idx(size_t i)const{ return particle_cells_[i]; }
	const Cell& cell(int i)const{ return cells_[i]; }
	double cutoff(void)const{ return cutoff_; }
	const std::vector<size_t> neighbours_of(size_t idx)const;
};






class NeighbourList{
private:
	// Pointers to a simulation box and a vector of particles in this box
	SimulationBox& box_;
	std::vector<Particle>& particles_;
	// Pointer to a cell list instance
	CellList& cell_list_;
	// Vector of vectors containing particle indices for each cell (a.k.a. a Verlet list)
	std::vector<std::vector<size_t>> particle_neighbours_;
	// Cutoff and padding that define the range of what we consider neighbours for particles
	// NOTE: this is larger than the particle interaction cutoff and thus than the cell list cutoff!
	double min_cutoff_, nb_cutoff_, nb_cutoff_sq_, nb_padding_;
	// Particle positions when the neighbour list was last built
	std::vector<Vector2d> old_positions_;
	// The two largest displacements since the list was last built.
	// Once the sum of these exceeds nb_padding, we should rebuild the list.
	double displacement_1_, displacement_2_;
	// A vector that tracks displacement from particles crossing the simulation volume
	std::vector<Vector2d> crossing_vecs_;
	// Current size of the containers: used to track changes in total number of particles
	size_t current_container_size_;

	void set_padding(void);
	void build_neighbour_list_through_cell_list(void);
	void build_neighbour_list_nsquared(void);
	void resize_containers(size_t n_elements);
public:
	NeighbourList(SimulationBox&, std::vector<Particle>&, CellList&, double);
	bool needs_update(const Particle&);
	void build(void);
	double padding(void)const{ return nb_padding_; }
	double min_cutoff(void)const{ return min_cutoff_; }
	double cutoff(void)const{ return nb_cutoff_; }
	size_t mean_nr_neighbours(void)const;

	bool are_neighbours(size_t i, size_t j);
	const std::vector<size_t>& neighbours_of(size_t i)const{ return particle_neighbours_[i]; }
	void add_crossing_vec(int i, Vector2d vec){ crossing_vecs_[i] += vec; /*printf("now %f %f %f\n",crossing_vecs[i][0],crossing_vecs[i][1],crossing_vecs[i][2]);*/}

	unsigned int builds=0;
};





















#endif
