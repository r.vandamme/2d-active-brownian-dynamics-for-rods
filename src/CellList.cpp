#include "CellList.h"

using Eigen::Vector2d;
using Eigen::Matrix2d;

// ============================== Cell list part ==============================

CellList::CellList(SimulationBox& box, std::vector<Particle>& particles, double cutoff)
	:
	box_(box), particles_(particles), cutoff_(cutoff)
{
	// Set the size of containers (just particle_cells_ atm)
	resize_containers( particles_.size() );
	// Build the cell list with the cutoff as suggested cell size
	build(cutoff_);
}

// Sets the size of particle_cells_ when # of particles changes
void CellList::resize_containers(size_t n_elements){
	// Resize
	particle_cells_.resize(n_elements);
}
// Converts coordinates into a cell number
size_t CellList::coords_to_cell(const Vector2d& pos){
	// Go to relative coordinates
	Vector2d rpos = box_.iL().array() * pos.array();
	// Calculate how far along each axis the particle is in relative coordinates
  size_t i = static_cast<size_t>(rpos[0] * nx_);
  size_t j = static_cast<size_t>(rpos[1] * ny_);
	// Give an error if index is out of range
	if( i >= nx_ || j >= ny_ || i < 0 || j < 0 ){
		printf("pos out of range => cell index out of range!\n");
		box_.print();
		printf("pos (%lf %lf)\n",pos[0],pos[1]);
		printf("x y nx ny: (%lu %lu), (%lu %lu)\n",i,j,nx_,ny_);
		exit(42);
	}
	// From there, calculate the index of the cell this position is in
  return ny_*i + j;
}
// Find the minimum number of cells we need to make sure each is larger than the minimum size
void CellList::find_size_and_create_cells(double suggested_cell_size){

	// Make sure cells are at least as large as the required cutoff
	if(suggested_cell_size < cutoff_){
		suggested_cell_size = cutoff_;
	}
	// Start cell size as large as they can be
	nx_ = floor(box_.L()[0] / suggested_cell_size);
	ny_ = floor(box_.L()[1] / suggested_cell_size);
	// Protect against small systems, make sure there is at least one cell
	if(nx_ == 0){ nx_ = 1; }
	if(ny_ == 0){ ny_ = 1; }
	// box_.print();
	// printf("suggested, cutoff, nx_ ny_ nz_ %f, %f, %d %d %d\n",suggested_cell_size,cutoff_,nx_,ny_,nz_);
	// Fix cells if they are too small, taking into account that the box might have a weird shape
	// cl->current_cell_size[0] = grow_cells(cl, box_.m[0], box_.m[1], &(cl->n_cells_xyz[0]), &(cl->n_cells_xyz[1]) );
	// cl->current_cell_size[1] = grow_cells(cl, box_.m[1], box_.m[2], &(cl->n_cells_xyz[1]), &(cl->n_cells_xyz[2]) );
	// cl->current_cell_size[2] = grow_cells(cl, box_.m[2], box_.m[0], &(cl->n_cells_xyz[2]), &(cl->n_cells_xyz[0]) );
	// Check whether we're liable to eat up too much memory
	if(nx_*ny_ > 1000000){
		printf("Huh. %lu cells. That'll take at least %lu MB of memory. ",nx_*ny_,nx_*ny_*sizeof(Cell)/(1024*1024));
		printf("But probably much more. Might freeze ya computar. Stopping now instead.\n");
		exit(42);
	}
	old_box_L_ = box_.L();

	// TODO Optimize this by resizing instead of complete rebuilding
	// Clear lists completely before rebuilding
	cells_.clear();
	// Make the cells
	for(size_t i = 0; i < nx_*ny_; i++){
		cells_.push_back( Cell(i) );
	}
}
// Find which cells neighbour which other cells and add those as eachothers neighbours
void CellList::find_cell_neighbours(void){
	// Link cells: find their neighbours and construct cell neighbour lists
  size_t i,j;
	int dx,dy; // these must be signed
	for(size_t x = 0; x < nx_; x++) for(size_t y = 0; y < ny_; y++){
		// Index of this cell
		i = x*ny_ + y;
		/* Clear neighbour list first */
		cells_[i].clear_neighbours();
		// printf("cells i nb: %d\n",cells_[i].n_neighbours());
		/* Now add neighbours (including itself) */
		for(int a = -1; a <= 1; a++) for(int b = -1; b <= 1; b++){
			dx = a; dy = b;
			// Periodicity
			if(((int) x)+dx < 0) dx = nx_-1;  else if(x+dx > nx_-1) dx = -nx_+1;
			if(((int) y)+dy < 0) dy = ny_-1;  else if(y+dy > ny_-1) dy = -ny_+1;
			// Index of neighbouring cell
			j = (x+dx)*ny_ + y+dy;
			// Check whether cell is already there (happens for #cells<9 due to periodicity)
			// If the neighbouring cell is not yet there, add it to the list
			if( cells_[i].contains_neighbour(j) == false ){
				cells_[i].add_neighbour(j);
				// printf("Added %d as nb of %d: %d now has %d nb\n",j,i,i,cells_[i].n_neighbours());
			}
		}
	}
}
// Sort all particles in the box into their appropriate cells
void CellList::sort_particles(void){
	for(Cell& c : cells_){
		// printf("cell %d has %d particles, ",c.index(),c.n_particles());
		c.clear_particles();
		// printf("%d after clearing\n",c.n_particles());
	}
	// Distribute particles into cells
	for(size_t i = 0; i < particles_.size(); i++){
		// Get reference to particle
		Particle& p = particles_[i];
		size_t j = coords_to_cell( p.pos() );
		// Get reference to cell
		Cell& c = cells_[j];
		if(c.n_particles() > particles_.size() / 1){
			printf("ERROR: Too many particles (%lu) in cell %lu (max %lu)!\n",c.n_particles(),c.index(),particles_.size()/1);
			printf("Total cells is %lu.\n",cells_.size());
			exit(42);
		}
		// Add particle to cell list and cell to particle_cell list
		c.add_particle(i);
		particle_cells_[i] = j;
		// printf("part %d to cell %d, now has %d\n",i,j,c.n_particles());
	}
}
// Returns a vector with all neighbours of the particle with index p_idx
const std::vector<size_t> CellList::neighbours_of(size_t p_idx)const{
	std::vector<size_t> all_p_neighbours;
	const Cell& c = this->cell( this->particles_cell_idx(p_idx) );
	const std::vector<size_t>& cell_neighbours = c.neighbours();
	for(size_t c_nb : cell_neighbours){
		std::vector<size_t> p_neighbours = this->cell(c_nb).particles();
		all_p_neighbours.insert(all_p_neighbours.end(), p_neighbours.begin(), p_neighbours.end());
	}
	return all_p_neighbours;
}


// Build a new cell list: find the size of the cells, link cell neighbours and then sort particles
void CellList::build(double suggested_cell_size){

	// Find the appropriate number of cells given a certain cutoff for particle interactions
	find_size_and_create_cells(suggested_cell_size);

	// Populate the list of neighbouring cells
	find_cell_neighbours();

	// Sort all particles into the cells based on their position
	sort_particles();
}
// This function rebuilds the parts of the cell list that need rebuilding
// That means rebuilding the cells if the box has changed, and always re-sorting the particles
void CellList::update(double suggested_cell_size){

	if(box_.L() != old_box_L_){
		// Gotta redo the whole thing, so just call build()
		build(suggested_cell_size);
	}else{
		// We can use the old cell partitioning of the box, just need to re-sort the particles
		sort_particles();
	}

}



// =========================== Verlet list part ===========================

NeighbourList::NeighbourList(
	SimulationBox& box,
	std::vector<Particle>& particles,
	CellList& cell_list_,
	double cutoff ) :
	box_(box), particles_(particles), cell_list_(cell_list_), min_cutoff_(cutoff), nb_padding_(1.0)
{
	// Initialize containers
	resize_containers( particles_.size() );
	// Set the old position to the current position for this first build
	for(Particle& p : particles_){
		old_positions_.push_back( p.pos() );
	}
	// Build the neighbour list
	build();
}

// Returns the average number of neighbours per particle
size_t NeighbourList::mean_nr_neighbours(void)const{
	unsigned int mean = 0;
	for(size_t i = 0; i < particle_neighbours_.size(); i++){
		mean += particle_neighbours_[i].size();
	}
	mean /= particle_neighbours_.size();
	return mean;
}
// Resizes old_positions_, particle_neighbours_ and crossing_vecs_ when # of particles changes
void NeighbourList::resize_containers(size_t n_elements){
	size_t old_n_elements = old_positions_.size();
	// Resize
	old_positions_.resize(n_elements);
	particle_neighbours_.resize(n_elements);
	crossing_vecs_.resize(n_elements,Vector2d::Zero());
	// Fill new elements if list grows
	for(size_t i = old_n_elements; i < n_elements; i++){
		old_positions_.push_back( particles_[i].pos() );
	}
	// Optionally, reserve some memory in advance for the number of neighbours.
	// There might be a smart way to guess an appropriate number for this based on the density.
	for(size_t i = old_n_elements; i < n_elements; i++){
		particle_neighbours_[i].reserve(50);
	}
	// Store the current size of the containers
	current_container_size_ = n_elements;
}
// Sets the distance for which we consider particles neighbours
void NeighbourList::set_padding(void){
	// A safety check
	if(cell_list_.cutoff() != min_cutoff_){
		printf("ERROR: Cell and neighbour list minimum cutoffs don't match!\n");
		exit(42);
	}
	// If particles are far from eachother, we can use a large padding. If they are
	// close, we should use a small one. So set one based on area per particle:
	// nb_padding_ = box_.area() / sqrt(particles_.size() * particles_[0].area());
	// nb_padding_ = 0.1 * min_cutoff_;
	nb_padding_ = box_.area() / (particles_.size() * particles_[0].area());
	// std::cout << nb_padding_ << '\n';

	// Make sure the padding doesn't become too small
	if(nb_padding_ < 0.1 * min_cutoff_){
		nb_padding_ = 0.1 * min_cutoff_;
	}
	// printf("padding is %f\n",nb_padding_);
	// Set the cutoff for the neighbour list
	nb_cutoff_ = min_cutoff_ + nb_padding_;
	nb_cutoff_sq_ = nb_cutoff_ * nb_cutoff_;
	// printf("nb list cutoff is %f\n",nb_cutoff_);
}
// Naive method of building the neighbour list. Inefficient, scales as O(N^2)
void NeighbourList::build_neighbour_list_nsquared(void){
	size_t N = particles_.size();
	// printf("nb list cutoff is %f\n",nb_cutoff_);

	for(size_t i = 0; i < N-1; i++){
		// Get reference to particle i
		Particle& pi = particles_[i];
		for(size_t j = i+1; j < N; j++){
			// Get reference to particle j
			Particle& pj = particles_[j];
			// Check square of periodic distance
			double dist_sq = box_.nearest_image_distancesq( pi.pos(), pj.pos() );
			// Compare this distance to cutoff, add to neighbour list if within cutoff radius
			// std::cout << "i,j: " << i << " " << j << " ";
			// std::cout << "dist, cutoff: " << sqrt(dist_sq) << " " << sqrt(nb_cutoff_sq_) << "\n";
			if(dist_sq < nb_cutoff_sq_){
				// std::cout << "Made " << i << " and " << j << " neighbours.\n";
				// std::cout << pi.pos()[0] << " " << pi.pos()[1] << " " << pi.pos()[2] << "\n";
				// std::cout << pj.pos()[0] << " " << pj.pos()[1] << " " << pj.pos()[2] << "\n";
				particle_neighbours_[i].push_back(j);
				particle_neighbours_[j].push_back(i);
			}
		}
		old_positions_[i] = pi.pos();
	}
	old_positions_[N-1] = particles_[N-1].pos();
}
// Builds neighbour list from cell list. Efficient, scales as O(N)
void NeighbourList::build_neighbour_list_through_cell_list(void){
	size_t N = particles_.size();
	// For each particle, look in neighbouring cells for neighbour candidates
	for(size_t i = 0; i < N; i++){
		Particle& pi = particles_[i];
		const std::vector<size_t> p_neighbours = cell_list_.neighbours_of(i);
		// Check for all these neighbours whether they are within cutoff of i
		for(size_t j : p_neighbours){
			if(i >= j){continue;} // No double counting
			Particle& pj = particles_[j];
			double dist_sq = box_.nearest_image_distancesq( pi.pos(), pj.pos() );
			if(dist_sq < nb_cutoff_sq_){
				// std::cout << "Added " << j << " as a neighbour of " << i << "\n";
				// std::cout << pi.pos()[0] << " " << pi.pos()[1] << " " << pi.pos()[2] << "\n";
				// std::cout << pj.pos()[0] << " " << pj.pos()[1] << " " << pj.pos()[2] << "\n";
				// We've found a neighbour, add it to the list
				particle_neighbours_[i].push_back(j);
				particle_neighbours_[j].push_back(i);
			}
		}
		// Save position of i when the list was built to track displacements.
		old_positions_[i] = pi.pos();
	}
}
// Returns whether two particles are neighbours or not
bool NeighbourList::are_neighbours(size_t idx_i, size_t idx_j){
	bool i_is_nb_of_j = false;
	bool j_is_nb_of_i = false;
	// std::cout << idx_i << ' ' << idx_j << '\n';
	for(size_t nb_i : particle_neighbours_[idx_i]){
		// std::cout << nb_i << ' ';
		if(idx_j == nb_i){ j_is_nb_of_i = true; break; }
	}
	// std::cout << '\n';
	for(size_t nb_j : particle_neighbours_[idx_j]){
		// std::cout << nb_j << ' ';
		if(idx_i == nb_j){ i_is_nb_of_j = true; break; }
	}
	// std::cout << "\n\n";
	return (i_is_nb_of_j && j_is_nb_of_i);
}


// Function that checks whether the Verlet list needs to be updated based on particle displacement
bool NeighbourList::needs_update(const Particle& p){
	// Get the position of the particle when the list was last made
	int idx = p.id();
	Vector2d& old_pos = old_positions_[idx];

	// If the distance to its old position is in the top two, make a new top two displacements
	// double dist = box_.nearest_image_distance( p.pos(), old_pos );
	double dist = (p.pos() - old_pos + crossing_vecs_[idx]).norm();
	if(dist > displacement_1_){
		displacement_2_ = displacement_1_;
		displacement_1_ = dist;
	}else if(dist > displacement_2_){
		displacement_2_ = dist;
	}

	// If the top two displacements are larger than the padding, we should rebuild the list
	if(displacement_1_ + displacement_2_ > nb_padding_){
		return true;
	}
	return false;
}
// General function to build neighbour list. Can add flags to specify when to use which method.
void NeighbourList::build(void){
	builds++;
	// printf("builds %d\n",builds);	box_.print();

	// We set a new padding for the neighbour list, based on the volume per particle
	set_padding();

  // We need a new cell list, call it and request a cell size equal to (or larger than) nb_cutoff_
	cell_list_.update(nb_cutoff_);

	// Reset the displacements and crossings, since we're rebuilding the list
	displacement_1_ = 0.0;
	displacement_2_ = 0.0;
	for(Vector2d& crossing_vec : crossing_vecs_){
		crossing_vec.setZero();
	}

	// Clear neighbour list before rebuilding and reserve some memory for new list
	for(auto& neighbours : particle_neighbours_){
		neighbours.clear();
		neighbours.reserve(32);
	}

	// Now choose a method to build the neighbour list
	// build_neighbour_list_nsquared();
	build_neighbour_list_through_cell_list();

	// Check that all nb pairs are symmetric
	// for(Particle& p : particles_){
	// 	for(size_t nb_idx : particle_neighbours_[p.id()]){
	// 		std::cout << are_neighbours(p.id(),nb_idx) << '\n';
	// 	}
	// }


}
