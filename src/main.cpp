#include "main.h"


// Make a struct that contains all the input parameters so we can easily pass it around.
Parameters simulation_parameters;


/* --------- Process all command line input ---------
 * Sorts command line input into their relevant simulation components.
 * Uses the "getopt" UNIX standard library.
 */
void parse_command_line_input(int argc, char *argv[]){
	int c;

  while(1){
    static struct option long_options[] =
      {
        /* These options set a flag. */
        // {"verbose", no_argument,       &verbose_flag, 1},
        // {"brief",   no_argument,       &verbose_flag, 0},
        /* These options don’t set a flag.
           We distinguish them by their indices. */
        {"Npart",       required_argument, 0, 'n'},
        {"Packfrac",    required_argument, 0, 'p'},
        {"AspectRatio", required_argument, 0, 'a'},
        {"Dtpara",      required_argument, 0, 'd'},
        {"Dtperp",      required_argument, 0, 'D'},
        {"Swimspeed",   required_argument, 0, 'v'},
        {"Totaltime",   required_argument, 0, 'T'},
        {"Timestep",    required_argument, 0, 't'},
				// This element of zeroes terminates the array
        {0, 0, 0, 0}
      };
    /* getopt_long stores the option index here. */
    int option_index = 0;

		// This parses the long name and converts it into its corresponding char
    c = getopt_long_only(argc, argv, "n:p:a:d:D:v:T:t:",
                    long_options, &option_index);

    /* Detect the end of the options. */
    if(c == -1) break;
    switch(c){
      case 0:
        /* If this option set a flag, do nothing else now. */
        if(long_options[option_index].flag != 0){ break; }
        printf("option %s", long_options[option_index].name);
        if(optarg){ printf (" with arg %s", optarg); }
        printf ("\n");
        break;

			// Sets number of particles
      case 'n':
				simulation_parameters.n_particles = strtol(optarg,NULL,10);
        break;
			// Sets packing fraction
      case 'p':
				simulation_parameters.packing_fraction = strtod(optarg,NULL);
        break;
			// Sets aspect ratio of the rods
      case 'a':
				simulation_parameters.lod = strtod(optarg,NULL);
        break;
			// Sets parallel diffusion constant
      case 'd':
				simulation_parameters.Dtpara = strtod(optarg,NULL);
        break;
			// Sets perpendicular diffusion constant
      case 'D':
				simulation_parameters.Dtperp = strtod(optarg,NULL);
        break;
			// Sets self-propulsion / swimming velocity
      case 'v':
				simulation_parameters.swim_speed = strtod(optarg,NULL);
        break;
			// Sets total time to simulate (in units of 1/Dr)
      case 'T':
				simulation_parameters.total_time = strtod(optarg,NULL);
        break;
			// Sets time step (in units of 1/Dr)
      case 't':
				simulation_parameters.time_step = strtod(optarg,NULL);
        break;

      case '?':
        /* getopt_long already printed an error message. */
				// We exit because we don't want to run invalid input.
				// TODO: add printhelp() here.
				exit(1);
        break;

      default:
        abort();
    }
  }

  /* Print any remaining command line arguments (not options). */
  if(optind < argc){
    printf("non-option ARGV-elements: ");
    while (optind < argc) printf("%s ", argv[optind++]);
    putchar('\n');
  }
}

/* --------- ======= MAIN ======= ---------
 *
 */
int main(int argc, char *argv[]){

	// Process command line input to split simulation and visualization input
	parse_command_line_input(argc, argv);

	// Initialize a simulation using the input parameters
	BD_Sim sim(simulation_parameters);
	// Run the simulation
	sim.run();

	return 1; // Simulation ended successfully
}
