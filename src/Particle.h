#ifndef BD_PARTICLE_H
#define BD_PARTICLE_H

#include <eigen3/Eigen/Geometry>
#include <memory>
#include "Shape.h"

using Eigen::Vector2d;

class Particle {
private:
  size_t index_; // index_ of this particle in the list of particles
  Vector2d pos_, prev_pos_; // vector of the x,y coordinates
  Vector2d rot_, prev_rot_; // x,y orientation vector

  Vector2d force_;
  double torque_=0.0;

  // Shape::Sphere shape_;
  Shape::Rod shape_;
  // Shape::Square shape_;
public:
  const double area(void)const{ return shape_.area(); }
  const double max_size(void)const{ return shape_.max_size(); }

  // Constructor for a sphere particle
  // Particle(int id, Vector2d pos, Vector2d rot, double diameter) :
	// 	index_(id), pos_(pos), rot_(rot), shape_(Shape::Sphere(1.0))
	// {}
    // const Shape::Sphere& shape(void)const{ return shape_; }

  // Constructor for a rod particle
  Particle(int id, Vector2d pos, Vector2d rot, double width, double length) :
		index_(id), pos_(pos), rot_(rot), shape_(Shape::Rod(width,length))
	{}
  const Shape::Rod& shape(void)const{ return shape_; }

  // Constructor for a square particle
  // Particle(size_t id, Vector2d pos, Vector2d rot, double edge_length) :
  // 	index_(id), pos_(pos), rot_(rot), shape_(Shape::Square(edge_length))
  // {}
  // const Shape::Square& shape(void)const{ return shape_; }

	// Destructor
  virtual ~Particle(void){};

  // Increment position and orientation
	void translate(const Vector2d& displacement){ pos_ += displacement; }
	void rotate(const double angle){
    // Convert from unit vector to angle
    double angle_ = atan2(rot_[1], rot_[0]);
    // Add rotation angle
    angle_ += angle;
    // New orientation unit vector
    rot_[0] = cos(angle_); rot_[1] = sin(angle_);
  };
	// Set properties
	void set_pos(const Vector2d& pos){ pos_ = pos; }
	void set_rot(const Vector2d& rot){ rot_ = rot; }
	void set_prev_pos(const Vector2d& pos){ prev_pos_ = pos; }
	void set_prev_rot(const Vector2d& rot){ prev_rot_ = rot; }
	void set_force(const Vector2d& force)  { force_ = force; }
	void set_torque(const double torque){ torque_ = torque; }
  void zero_force_and_torque(void){ force_.setZero(); torque_ = 0.0; }
	// Get properties
	const size_t id(void)const { return index_; }
	const Vector2d& pos(void)const { return pos_; }
	const double x(void)const{ return pos_[0]; }
	const double y(void)const{ return pos_[1]; }
	const Vector2d& rot(void)const { return rot_; }
	const double nx(void)const{ return rot_[0]; }
	const double ny(void)const{ return rot_[1]; }
	const Vector2d& prev_pos(void)const { return prev_pos_; }
	const Vector2d& prev_rot(void)const { return prev_rot_; }
	Vector2d& force(void) { return force_; }
	double& torque(void){ return torque_; }

};

#endif
