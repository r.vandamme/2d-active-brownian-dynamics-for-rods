#include "Shape.h"

using Eigen::Vector2d;
using Eigen::Matrix2d;
using Eigen::Vector2i;

// Suppress a false warning from certain gcc versions (only 5.4 known to have a problem atm)
#if (__GNUC__ == 5 && __GNUC_MINOR__ == 4)
#pragma GCC diagnostic ignored "-Warray-bounds"
#endif


namespace Shape {

  // Function that gives closest distance between and closest points of two line segments
  void line_line_distance(
    const double hl_1, const double hl_2,
    const Vector2d& rot_1, const Vector2d& rot_2,
    const Vector2d& cm_vec,
    double& dist_sq,
    double& la_1, double& la_2
  ){
    double rr = cm_vec.squaredNorm();
    double ru1 = cm_vec.dot(rot_1);
    double ru2 = cm_vec.dot(rot_2);
    double u1u2 = rot_1.dot(rot_2);
    double cc = 1.0 - u1u2*u1u2;
    if(cc < 1e-10){ // If nearly exactly parallel, avoid some numerical errors
      if(ru1 && ru2){
        la_1 = ru1/2;
        la_2 = -ru2/2;
      }else{ // Avoid a special error
        la_1 = 0.0;
        la_2 = 0.0;
        dist_sq = rr;
        return;
      }
    }else{
      // Step 1
      la_1 = (ru1-u1u2*ru2)/cc;
      la_2 = (-ru2+u1u2*ru1)/cc;
    }
    // Step 2
    if( fabs(la_1) > hl_1 || fabs(la_2) > hl_2 ){
      // Step 3 - 7
      if( fabs(la_1)-hl_1 > fabs(la_2)-hl_2 ){
        la_1 = copysign(hl_1,la_1);
        la_2 = la_1*u1u2 - ru2;
        if( fabs(la_2) > hl_2 ){
          la_2 = copysign(hl_2,la_2);
        }
      }else{
        la_2 = copysign(hl_2,la_2);
        la_1 = la_2*u1u2 + ru1;
        if( fabs(la_1) > hl_1 ){
          la_1 = copysign(hl_1,la_1);
        }
      }
    }
    dist_sq = rr + la_1*la_1 + la_2*la_2 + 2.0 * (la_2*ru2 - la_1 * (ru1+la_2*u1u2));
    return;
  }

  // Shortest distance between two rods is same as between two line segments
  void Rod::approach(
    const double hl_1, const double hl_2,
    const Vector2d& rot_1, const Vector2d& rot_2,
    const Vector2d& cm_vec,
    double& dist_sq,
    double& la_1, double& la_2
  )const{
    line_line_distance(hl_1, hl_2, rot_1, rot_2, cm_vec, dist_sq, la_1, la_2);
  }

  // Function that gives closest distance between and closest points of two squares
  void Square::approach(
    const Square& other,
    const Vector2d& rot_1, const Vector2d& rot_2,
    const Vector2d& cm_vec,
    double& dist_sq,
    Vector2d& arm_1, Vector2d& arm_2
  )const{
    std::array<Vector2d,4> v_1, v_2;
    double proj_v_1[4], proj_v_2[4];
    // Rotate vertices by the orientation of the particles and project onto cm_vec
    for(size_t j,i = 0; i < 4; i++){
      v_1[i] = (Matrix2d() << rot_1[0], rot_1[1], -rot_1[1], rot_1[0]).finished() * this->vertex_[i];
      v_2[i] = (Matrix2d() << rot_2[0], rot_2[1], -rot_2[1], rot_2[0]).finished() * other.vertex_[i];
      // Don't forget to translate vertices of square 2
      v_2[i] += cm_vec;
      proj_v_1[i] = v_1[i].dot(cm_vec);
      proj_v_2[i] = v_2[i].dot(cm_vec);
      // NOTE : the two while-s below trigger a false warning on some gcc versions.
      // Sort v_1 in descending order (largest first)
      j = i;
      while(j > 0 && proj_v_1[j] > proj_v_1[j-1]){
        std::swap(proj_v_1[j-1], proj_v_1[j]);
        // std::swap(indices_1[j-1], indices_1[j]);
        std::swap(v_1[j-1], v_1[j]);
        j--;
      }
      // Sort v_2 in ascending order (smallest first)
      j = i;
      while(j > 0 && proj_v_2[j] < proj_v_2[j-1]){
        std::swap(proj_v_2[j-1], proj_v_2[j]);
        // std::swap(indices_2[j-1], indices_2[j]);
        std::swap(v_2[j-1], v_2[j]);
        j--;
      }
    }

    // Brute force method: just calculate all edge pairs and get nearest distance
    // from there. NOTE: this messes up torque arms if they overlap.
    // Vector2d rot_e_1, rot_e_2, c_to_c;
    // double la_1, la_2, ee_dist_sq, min_ee_dist_sq = 1E10;
    // for(size_t i = 0; i < 4; i++){
    //   rot_e_1 = (v_1[(i+1)>3?0:(i+1)] - v_1[i]).normalized();
    //   for(size_t j = 0; j < 4; j++){
    //     rot_e_2 = (v_2[(j+1)>3?0:(j+1)] - v_2[j]).normalized();
    //     c_to_c = (v_2[(j+1)>3?0:(j+1)] - (v_2[j].array() * 0.5).matrix()) -
    //              (v_1[(i+1)>3?0:(i+1)] - (v_1[i].array() * 0.5).matrix());
    //     // Now calculate the edge-edge distance and store the smallest one
    //     Shape::line_line_distance(
    //       this->hl(), other.hl(),
    //       rot_e_1, rot_e_2, c_to_c,
    //       ee_dist_sq, la_1, la_2
    //     );
    //     if(ee_dist_sq < min_ee_dist_sq){
    //       min_ee_dist_sq = ee_dist_sq;
    //       // Also calculate and store the torque arms
    //       arm_1 = (rot_e_1.array() * la_1) + (v_1[(i+1)>3?0:(i+1)].array() - v_1[i].array() * 0.5);
    //       arm_2 = (rot_e_2.array() * la_2) + (v_2[(j+1)>3?0:(j+1)].array() - v_2[j].array() * 0.5);
    //     }
    //   }
    // }
    // dist_sq = min_ee_dist_sq;
    // return;


    // If farthest of 1 is farther than nearest of 2, they overlap, in
    // which case we need to take special care with the torque arms.
    Vector2d rot_e_1, rot_e_2, c_to_c;
    double la_1, la_2, ee_dist_sq;
    if(proj_v_1[0] < proj_v_2[0]){ // If no overlap
      // Take the two edges formed by the closest two pairs of vertices and calculate distance
      Vector2d rot_e_1 = (v_1[1] - v_1[0]).normalized();
      Vector2d rot_e_2 = (v_2[1] - v_2[0]).normalized();
      Vector2d c_to_c = (v_2[1] - (v_2[0].array() * 0.5).matrix()) -
                        (v_1[1] - (v_1[0].array() * 0.5).matrix());
      double la_1, la_2;
      line_line_distance(
        this->hl(), other.hl(),
        rot_e_1, rot_e_2, c_to_c,
        dist_sq, la_1, la_2
      );
      // Calculate the torque arms
      arm_1 = (rot_e_1.array() * la_1) + (v_1[1].array() - v_1[0].array() * 0.5);
      arm_2 = (rot_e_2.array() * la_2) + (v_2[1].array() - v_2[0].array() * 0.5);
      return;
    }else{
      // If they overlap, the torque arm is the mean of the 2 edge-edge intersections
      // There's two cases: 2 edges overlap with 1, or 2 edges overlap with 2.
      // If you need to understand this, draw the two ways two squares can overlap.
      // Tip: projected onto cm_vec, square 1 is always < square 2.
      if(proj_v_1[1] > proj_v_2[0]){ // Edge square 1 overlaps with 2 edges of square 2
        arm_1.setZero();
        arm_2.setZero();
        rot_e_1 = (v_1[1] - v_1[0]).normalized();
        for(size_t j = 0; j < 2; j++){
          rot_e_2 = (v_2[j+1] - v_2[j]).normalized();
          c_to_c = (v_2[j+1] - (v_2[j].array() * 0.5).matrix()) -
                   (v_1[1] - (v_1[0].array() * 0.5).matrix());
          // Now calculate the edge-edge distance and store the smallest one
          Shape::line_line_distance(
            this->hl(), other.hl(),
            rot_e_1, rot_e_1, c_to_c,
            ee_dist_sq, la_1, la_2
          );
          // Calculate the torque arms by averaging over the 2 contacts
          arm_1.array() += (rot_e_1.array() * la_1) + (v_1[1].array() - v_1[0].array() * 0.5);
          arm_1 /= 2;
          arm_2.array() += (rot_e_2.array() * la_2) + (v_2[j+1].array() - v_2[j].array() * 0.5);
          arm_2 /= 2;
          dist_sq = 0;
          return;
        }
      }else if(proj_v_2[1] < proj_v_1[0]){ // Edge square 2 overlaps with 2 edges of square 1
        arm_1.setZero();
        arm_2.setZero();
        rot_e_2 = (v_2[1] - v_2[0]).normalized();
        for(size_t i = 0; i < 2; i++){
          rot_e_1 = (v_1[i+1] - v_1[i]).normalized();
          c_to_c = (v_2[1] - (v_2[0].array() * 0.5).matrix()) -
                   (v_1[i+1] - (v_1[i].array() * 0.5).matrix());
          // Now calculate the edge-edge distance and store the smallest one
          Shape::line_line_distance(
            this->hl(), other.hl(),
            rot_e_1, rot_e_1, c_to_c,
            ee_dist_sq, la_1, la_2
          );
          // Calculate the torque arms by averaging over the 2 contacts
          arm_1.array() += (rot_e_1.array() * la_1) + (v_1[i+1].array() - v_1[i].array() * 0.5);
          arm_1 /= 2;
          arm_2.array() += (rot_e_2.array() * la_2) + (v_2[1].array() - v_2[0].array() * 0.5);
          arm_2 /= 2;
          dist_sq = 0;
          return;
        }
      }else{ // 2 Edges square 2 overlap with 2 edges square 1
        arm_1.setZero();
        arm_2.setZero();
        for(size_t i = 0; i < 2; i++){
          rot_e_1 = (v_1[i+1] - v_1[i]).normalized();
          for(size_t j = 0; j < 2; j++){
            rot_e_2 = (v_2[j+1] - v_2[j]).normalized();
            c_to_c = (v_2[j+1] - (v_2[j].array() * 0.5).matrix()) -
                     (v_1[i+1] - (v_1[i].array() * 0.5).matrix());
            // Now calculate the edge-edge distance and store the smallest one
            Shape::line_line_distance(
              this->hl(), other.hl(),
              rot_e_1, rot_e_1, c_to_c,
              ee_dist_sq, la_1, la_2
            );
            // Calculate the torque arms by averaging over the 2 contacts
            if(ee_dist_sq <= 1E-10){ // Only count overlapping edge pairs (2 out of 4)
              arm_1.array() += (rot_e_1.array() * la_1) + (v_1[i+1].array() - v_1[i].array() * 0.5);
              arm_1 /= 2;
              arm_2.array() += (rot_e_2.array() * la_2) + (v_2[j+1].array() - v_2[j].array() * 0.5);
              arm_2 /= 2;
            }
            dist_sq = 0;
            return;
          }
        }
      }
    }

  }
}
