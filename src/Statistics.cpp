#include "Statistics.h"


namespace statistics{

	/*========================================= OBSERVABLE =========================================*/

	/* --------- Resets an observable to zero measurements ---------
	 *
	 */
	void Observable::clear(void){
		samples_   = 0;
		current_   = 0.0;
		mean_      = 0.0;
		mvariance_ = 0.0;
	}
	/* --------- Updates an observable after its current value is changed ---------
	 * For expressions of incremental averages and variance, see
	 * http://people.ds.cam.ac.uk/fanf2/hermes/doc/antiforgery/stats.pdf
	 * or just google it.
	 */
	void Observable::add_measurement(double value){
		current_ = value;
		double prev_mean = mean_;
		samples_++;
		mean_ += ( 1.0/ ((double) samples_) ) * (value - mean_);
		if(samples_ > 1){
			mvariance_ += (value - prev_mean) * (value - mean_);
		}
		// 	printf("    current value: %lf\n",value);
		// 	printf("       mean value: %lf\n",mean_);
		// 	printf("  prev_mean value: %lf\n",prev_mean);
		// 	printf("  mvariance value: %lf\n",mvariance_);
		// 	printf("     # of samples: %ld\n",samples_);
	}
	/* --------- Appends the current mean and variance to a file ---------
	 *
	 */
	void Observable::save_current_append_to_file(const std::string file_name, double time){
		static bool first = true;
		std::ofstream file;
		if(first){
			// Open a file to write to in truncate mode (make a new file!)
		  file.open(file_name, std::ios::out | std::ios::trunc);
			first = false;
		}else{
			// Open a file to write to in append mode
		  file.open(file_name, std::ios::out | std::ios::app);
		}
		// Set output stream precision to 10 significant figures
		file.precision(10);
		// Save the data
		file << time <<' '<< current_ <<'\n';
		// Close file stream
		file.close();
	}
	/* --------- Save the current mean and variance to a file ---------
	 *
	 */
	void Observable::save_mean_to_file(const std::string file_name){
		// Open a file to write to in truncate mode (makes a new file / wipes old one)
		std::ofstream file;
		file.open(file_name, std::ios::out | std::ios::trunc);
		// Set output stream precision to 10 significant figures
		file.precision(10);
		// Save the data
		file << mean_ <<' '<< mvariance_/samples_ <<'\n';
		// Close file stream
		file.close();
	}

	/*========================================= HISTOGRAM ==========================================*/

	// Constructor if we want a specific number of bins
	Histogram::Histogram(size_t n_bins, double startx, double endx){
		initialize_with_size(n_bins, startx, endx);
	}
	// Constructor if we want a specific binwidth
	Histogram::Histogram(double binwidth, double startx, double endx){
		initialize_with_size(binwidth, startx, endx);
	}
	// Size setters to use if histogram size isn't known upon declaration
	void Histogram::initialize_with_size(size_t n_bins, double startx, double endx){
		if(initialized_){
			std::cout << "Error: double histogram initialization. Exiting.\n";
			exit(42);
		}
		startx_ = startx;
		endx_ = endx_;
		binwidth_ = (endx-startx) / ((double) n_bins);
		// Resize to the appropriate number of bins
		values_.resize(n_bins);
		// Then initialize elements
		for(size_t i = 0; i < n_bins; i++){
			// Set all yvalues to zero to make sure there's no weird leftover bits from memory
			values_[i].clear();
		}
		initialized_ = true;
	}
	void Histogram::initialize_with_size(double binwidth, double startx, double endx){
		if(initialized_){
			std::cout << "Error: double histogram initialization. Exiting.\n";
			exit(42);
		}
		if( endx - startx < binwidth ){
			std::cout << "Error: histogram domains is smaller than binwidth! Exiting.\n";
			exit(42);
		}
		binwidth_ = binwidth;
		startx_ = startx;
		endx_ = endx_;
		size_t n_bins = (endx - startx) / binwidth;
		// Resize to the appropriate number of bins
		values_.resize(n_bins);
		// Then initialize elements
		for(size_t i = 0; i < n_bins; i++){
			// Set all yvalues to zero to make sure there's no weird leftover bits from memory
			values_[i].clear();
		}
		initialized_ = true;
	}
	// Add a measurement if we know which bin it falls into
	void Histogram::add_measurement(size_t bin, double yvalue){
		if( bin > values_.size() ){
			printf("Error: bin exceeds size of histogram!	Maybe histogram is uninitialized?	\n");
			exit(42);
			return;
		}
		values_[bin].add_measurement(yvalue);
	}
	// Add a measurement if we only have an xvalue instead
	void Histogram::add_measurement(double xvalue, double yvalue){
		size_t bin = (xvalue - startx_) / binwidth_;
		// std::cout << xvalue << ' ' << bin << ' ' << yvalue << '\n';
		add_measurement(bin, yvalue);
	}
	// Clears all observables in the histogram, but keeps xvalues
	void Histogram::clear(void){
		// Reset all measured values
		for(Observable& value : values_){	value.clear(); }
	}
	// Save histogram to a file
	void Histogram::save_to_file(std::string file_name)const{
		// Make a new file to write to, overwrite if it exists
		std::ofstream file;
		file.open(file_name, std::ios::out | std::ios::trunc);
		// Save all xvalues and yvalues to the file
		for(size_t i = 0; i < values_.size(); i++){
			double xvalue = startx_ + ((double) i + 0.5) * binwidth_;
			file << xvalue << ' ' << values_[i].mean() << ' ' << values_[i].stdev() << '\n';
		}
		file.close();
	}

	/*===================================== DYNAMIC HISTOGRAM ======================================*/

	// Constructor that sets binwidth and initial size
	DynamicHistogram::DynamicHistogram(size_t n_bins, double binwidth, double startx){
		initialize(n_bins, binwidth, startx);
	}
	// Size setters to use if histogram size isn't known upon declaration
	void DynamicHistogram::initialize(size_t n_bins, double binwidth, double startx){
		if(initialized_){
			std::cout << "Error: double histogram initialization. Exiting.\n";
			exit(42);
		}
		startx_ = startx;
		binwidth_ = binwidth;
		// Resize vectors to the appropriate number of bins
		values_.resize(n_bins);
		// Then initialize elements
		for(size_t i = 0; i < n_bins; i++){
			// Set all yvalues to zero to make sure there's no weird leftover bits from memory
			values_[i].clear();
		}
		initialized_ = true;
	}
	// Gives the bin corresponding to xvalue. If out of range, gives warning and retuns last bin.
	size_t DynamicHistogram::bin_of(double xvalue)const{
		if( xvalue < startx_ ){
			std::cout << "Error: requested xvalue is below starting xvalue of histogram!\n";
			exit(42);
		}else if( xvalue - startx_ < binwidth_ * values_.size() ){
			return floor((xvalue - startx_) / binwidth_);
		}else{
			std::cout << "Warning: requested bin out of range! Returning last bin.\n";
			return values_.size();
		}
	}
	// Gives xvalue of bin. If bin is out of range, gives warning and returns max xvalue
	double DynamicHistogram::xvalue_of(size_t bin)const{
		if( bin > values_.size() ){
			std::cout << "Warning: requested bin out of range! Returning xvalue of last bin.\n";
		}
		return bin * binwidth_ + startx_;
	}
	// Add a measurement if we know which bin it falls into
	void DynamicHistogram::add_measurement(size_t bin, double yvalue){
		if( bin > values_.size() ){
			return;
			// std::cout << "Error: bin (" << bin << ") ";
			// std::cout << "exceeds size of histogram (" << values_.size() << ")!\n";
			// exit(42);
			// return;
		}
		values_[bin].add_measurement(yvalue);
	}
	// Add a measurement if we only have an xvalue instead
	void DynamicHistogram::add_measurement(double xvalue, double yvalue){
		size_t bin = (xvalue - startx_) / binwidth_;
		// std::cout << xvalue << ' ' << bin << ' ' << yvalue << '\n';
		add_measurement(bin, yvalue);
	}
	// Clears all observables in the histogram, but keeps xvalues
	void DynamicHistogram::clear(void){
		// Reset all measured values
		for(Observable& value : values_){	value.clear(); }
	}
	// Grows the histogram to a new size
	void DynamicHistogram::grow_to_size(size_t new_n_bins){
		size_t old_n_bins = values_.size();
		if(new_n_bins < old_n_bins){
			printf("Error: attempting to shrink histogram, could yield undefined behaviour.\n");
			exit(42);
		}
		// Resize to the appropriate number of bins
		values_.resize(new_n_bins);
		// Then initialize elements
		for(size_t i = old_n_bins; i < new_n_bins; i++){
			// Set all yvalues to zero to make sure there's no weird leftover bits from memory
			values_[i].clear();
		}
	}
	// Grows the histogram to include the bin that has the input xvalue
	void DynamicHistogram::grow_to_include(double xvalue){
		if(xvalue < startx_){
			std::cout << "Error: Requested growing histogram to xvalue < starting xvalue!\n";
			exit(42);
		}
		size_t new_n_bins = (xvalue - startx_) / binwidth_ + 1;
		grow_to_size(new_n_bins);
	}
	// Save histogram to a file
	void DynamicHistogram::save_to_file(std::string file_name)const{
		// Make a new file to write to, overwrite if it exists
		std::ofstream file;
		file.open(file_name, std::ios::out | std::ios::trunc);
		// Save all xvalues and yvalues to the file
		for(size_t i = 0; i < values_.size(); i++){
			double xvalue = startx_ + ((double) i + 0.5) * binwidth_;
			file << xvalue << ' ' << values_[i].mean() << ' ' << values_[i].stdev() << '\n';
		}
		file.close();
	}


}
