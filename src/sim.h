#ifndef BD_SIM_H
#define BD_SIM_H

#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <fstream>
#include <memory>
#include <stack>
#include <eigen3/Eigen/Geometry>
#include "rnd.h"
#include "RectangularSimulationBox.h"
#include "Particle.h"
#include "Statistics.h"
#include "CellList.h"
#include "voro++.hh"

using Eigen::Vector2d;
using Eigen::Matrix2d;

// The nondimensionalization needs three things: a length scale, an energy scale and a time scale.
// It is conventient to set the length scale to either the diameter or the length of the rods, the
// energy scale to the thermal energy and the time scale to one over the rotational diffusion.


// This struct contains all the simulation parameters we want to pass to the simulation.
struct Parameters {
	unsigned int n_particles=1000; // Number of particles
	double packing_fraction=0.45;  // Fraction of volume occupied by particles

	double lod=2.0;    // Aspect ratio for all rods.
	double Dtpara=1.0/3.0; // Translational diffusion constant parallel to rod orientation for all rods.
	double Dtperp=1.0/3.0; // Translational diffusion constant perpendicular to rod orientation for all rods.

	double swim_speed = 100.0;

	double WCA_steepness = 24.0; // WCA potential strength, commonly indicated by beta epsilon

	double aspect_ratio_box = 1.0; // Aspect ratio of simulation box. The x axis can be elongated.
	double total_time = 100.0;
	double time_step = 1E-4;
	double init_time = 50.0;

	unsigned int n_snapshots = 1000; // Number of snapshots to save to a file
};




class BD_Sim {
private:
	// Essential components: Particles, simulation volume and random number generator
	SimulationBox box;               // Simulation volume
	std::vector<Particle> particles; // Vector that stores all the particles
	tRnd RNG;                        // Using C++11's Marsenne Twister RNG
	// Optimization components: cell and neighbour lists for efficient particle interactions
	CellList* cell_list;             // Currently only used to make the neighbour list
	// Many measurements are based on Voronoi tesselation, so make a Voro++ copy of simulation box
	voro::container *voro_box;
	// Simulation parameters
	Parameters parameter_list;       // These are given as input
	double cutoff, cutoff_sq;        // Distances < cutoff are considered for force calculations
	double wca_prefactor;            // Steepness of the repulsive WCA potential
	// Simulation time parameters
	double t;                        // Current time
	double total_time;               // Total time to simulate in units of 1/Dr
	// unsigned int total_steps;        // Total steps to simulate
	double dt;                       // Integration time step, expressed in units of 1/Dr
	// Other things
	unsigned int n_snapshots = 100;
	statistics::Observable fraction_truncated;   // Fraction of steps that needed to have the force/torque truncated
	// Maximum force, translation and rotation per time step, used to prevent 1/r^n potential instabilities
	double max_force;
	double max_translation_sq;
	double max_rotation_sq;
	// Simulation integration constants
	double ic_A;
	double ic_F_para;
	double ic_F_perp;
	double ic_T;
	double ic_B_para;
	double ic_B_perp;
	double ic_B_rota;

public:
	BD_Sim(Parameters);
	virtual ~BD_Sim(void){
		delete cell_list;
		delete voro_box;
	};

	// Prepares the simulation. Creates a box, fills it with particles and makes a cell/nb list
	void initialize(void);

	// Sets the time step and all related constants
	void set_time_step(double);

	// Actually runs the simulation after it is initialized
	void run(void);

	// Force calculations
	void calc_force_2spheres_WCA(Particle&, Particle&);
 	void calc_force_and_torque_2rods_WCA(Particle&, Particle&);
 	void calc_force_and_torque_2squares_WCA(Particle&, Particle&);
	void calc_forces(void);

	// Equation of motion integrators
	void integrate_Euler_Maruyama(void);
	void integrate_BAOAB(void);

	// Enforce periodicity
	bool put_in_box(Particle& p, Eigen::Vector2d& to_box);

	// Sorts particles into clusters based on interparticle distances
	statistics::Observable max_cluster_fraction; // fraction of particles in largest cluster
	void calc_clusters(std::vector<size_t>& cluster_idx, statistics::Observable& max_cluster_frac, double cutoff);
	void calc_clusters_density(std::vector<size_t>& cluster_idx, statistics::Observable& max_cluster_frac, double cutoff);
	// Saves the current particle configuration to a file
	void save_snapshot(const std::string name);

	// Anything we want to measure goes here
	statistics::Histogram hist_gr;
	void measure_gr(statistics::Histogram& hist);

	statistics::DynamicHistogram hist_or_corr;
	void measure_orientational_autocorrelation(statistics::DynamicHistogram& hist, double time);

	statistics::Observable effective_velocity;
	void measure_effective_velocity(statistics::Observable& obs);

	statistics::Histogram hist_local_packfrac;
	void voronoi_cell_areas_Voro(std::vector<double>& cell_areas);
	void measure_local_packfrac_Voro(statistics::Histogram& hist);
};

#endif
