#include "sim.h"

BD_Sim::BD_Sim(Parameters input_parameters) : parameter_list(input_parameters) {
	initialize();
}
/* --------- Prepare the simulation for running ---------
 *
 */
void BD_Sim::initialize(void){
	Parameters params = parameter_list;

	// Seed the random number generator
	RNG.seed(time(NULL));
	// Also seed the standard RNG that Eigen uses
	std::srand(time(NULL));

	// Set how many snapshots should be made
	n_snapshots = params.n_snapshots;

	// Set the box dimensions so the box aspect ratio and density are correct
	double Apart = params.n_particles * (M_PI/4.0 + (params.lod-1.0)); // spheres or rods
	// double Apart = params.n_particles * 1.0;
	double density = params.n_particles * params.packing_fraction / Apart;
	double sbox = 1.0, lbox = sbox * params.aspect_ratio_box;
	double resizefactor = sqrt( params.n_particles / (density*lbox*sbox) );
	lbox *= resizefactor; sbox *= resizefactor;
	box.set(lbox, sbox);

	// Fill the box with particles at random positions and with random orientations
	for(size_t i = 0; i < params.n_particles; i++){
		Vector2d random_pos = box.L().array() * ((Vector2d::Random().array()+1.0)/2.0);
		Vector2d random_rot = Vector2d::Random().normalized();
		// Vector2d random_rot = Vector2d(RNG.rani(0,0),RNG.rani(-1,1)).normalized();
		// particles.push_back( Particle(i, random_pos, random_rot, 1.0) ); // if spheres
		particles.push_back( Particle(i, random_pos, random_rot, 1.0, params.lod) ); // if rods
		// particles.push_back( Particle(i, random_pos, random_rot, 1.0) ); // if squares
	}

	// TEMP
	// particles.push_back(
	// 	Particle(0,	(Vector2d(0,0).array()*2*params.lod).matrix() + Vector2d(0.5*lbox,0.5*sbox), Vector2d(0.866025, 0.5),	1.0, params.lod)
	// );
	// particles.push_back(
	// 	Particle(1,	(Vector2d(1,0).array()*2*params.lod).matrix() + Vector2d(0.5*lbox,0.5*sbox), Vector2d(-0.866025,0.5),	1.0, params.lod)
	// );
	// particles.push_back(
	// 	Particle(2,	(Vector2d(0.5,sqrt(3)/2).array()*2*params.lod).matrix() + Vector2d(0.5*lbox,0.5*sbox), Vector2d(0,-1),	1.0, params.lod)
	// );

	// Set the particle's diffusion constants
	// double Dtpara = params.Dtpara;
	// double Dtperp = params.Dtperp;
	// if(params.lod==0 && params.Dtpara==0 && params.Dtperp==0){ // no params set
	// 	std::cout << "Insufficient input parameters.\n";
	// }
	// else if(params.lod!=0 && params.Dtpara==0 && params.Dtperp==0){ // Only lod set
	// 	std::cout << "Calculating diffusion constants from aspect ratio and known literature.\n";
	// 	double lod = params.lod;
	// 	// Set all diffusion constants by using literature expressions for certain aspect ratio
	// 	if(lod <= 2.0){ // For short rods, use Perrin's ellipsoid expressions
	// 		// TODO
	// 		double xi = sqrt(lod*lod - 1.0) / lod;
	// 		double Perrin_factor = 2.0 * atanh(xi) / xi;
  //
	// 	}else{ // Long rods, use cylinder eqns. from DOI's: 10.1063/1.447827, 10.1103/PhysRevE.50.1232
	// 		double Dtpara_factor = (1./(2*M_PI))*(log(lod)-0.207+0.980/lod-0.133/(lod*lod));
	// 		double Dtperp_factor = (1./(4*M_PI))*(log(lod)+0.839+0.185/lod+0.233/(lod*lod));
	// 		double Dr_factor     = (3./(M_PI*lod*lod))*(log(lod)-0.662+0.917/lod-0.050/(lod*lod));
	// 		// We nondimensionalize with Dr==1 and rod diameter==1, so Dtparr and Dtperp in those units:
	// 		Dtpara = Dtpara_factor / Dr_factor;
	// 		Dtpara = Dtperp_factor / Dr_factor;
	// 	}
	// }

	// Calculate the integration constants (A active, F force, T torque, B brownian)
	// Set the total time and time step to simulate
	total_time = params.total_time;
	set_time_step(params.time_step);

	// Calculate the cutoff length for the particle interactions
	cutoff = 0.0;
	for(Particle& p : particles){
		// WCA interaction length is 2^(1/6), so cutoff should be length - sigma(1) + that
		double l_interaction = p.max_size() - 1.0 + pow(2.0, 1.0/6.0);
		if( l_interaction > cutoff ){ cutoff = l_interaction; }
	}
	cutoff_sq = cutoff * cutoff;

	// Set the maximum force, translation and rotation parameters
	wca_prefactor = 48.0 * params.WCA_steepness;
	double r2i = pow(1E-6,-2), r6i = pow(1E-6,-6);
	max_force = wca_prefactor * r2i * r6i * (r6i-0.5);
	max_translation_sq = pow(0.1, 2);
	max_rotation_sq = pow(0.2 * 0.5 * M_PI / params.lod, 2);

	// Create the cell list, and the neighbour list if we're using that
	cell_list = new CellList(box, particles, cutoff);

	// Create the Voro++ box
	double max_x = box.L()[0]; // , min_x = 0;
	double max_y = box.L()[1]; // , min_y = 0;
	// Find a very rough number of subregions to divide the box into for numerical efficiency
	// and because this is a required input parameter.
	int n_x = floor(max_x);
	int n_y = floor(max_y);
	int n_z = 1;
	// Create a container with dimensions given by x/y/z_min/max, periodic
	// in 3 dimensions, with 8 particles per computational block (this is
	// not very important). Set z size to 1 so it works in 2D.
	voro_box = new voro::container(0, max_x, 0, max_y, 0, 1, n_x, n_y, n_z, true, true, true, 8);
}
/* --------- Changes the time step and all related constants ---------
 *
 */
void BD_Sim::set_time_step(double new_dt){
	if(new_dt < 0){ std::cout << "Error: negative time step!\n"; exit(42); }
	dt = new_dt;
	// Calculate the integration constants (A active, F force, T torque, B brownian)
	ic_A      = parameter_list.swim_speed * dt;
	ic_F_para = 48.0 * parameter_list.WCA_steepness * dt;// * Dtpara; leaving out these terms breaks fluctuation-
	ic_F_perp = 48.0 * parameter_list.WCA_steepness * dt;// * Dtperp; dissipation, but does simplify things.
	ic_T      = 48.0 * parameter_list.WCA_steepness * dt; // Dr is used to nondimensionalize so always equals 1
	ic_B_para = sqrt(2.0 * dt * parameter_list.Dtpara);
	ic_B_perp = sqrt(2.0 * dt * parameter_list.Dtperp);
	ic_B_rota = sqrt(2.0 * dt); // Dr is used to nondimensionalize so always equals 1
}

/* --------- Calculate the force and torque between two spheres ---------
 *
 */
void BD_Sim::calc_force_2spheres_WCA(Particle& pi, Particle& pj){
	// Nearest image convention for CM distance
	Vector2d near_vec = box.nearest_image_vector( pi.pos(), pj.pos() );
	double r2 = near_vec.squaredNorm();
	// Calculate forces and torques, only nonzero for r_min/d < 1.0, according to WCA potential
	if(r2 < 1.2599210498948731648){ // 2^(1/3) = (2^(1/6))^2 = 1.2599210498948731648
		// WCA Force calculation
		double r2i = 1.0 / r2;
		double r6i = r2i*r2i*r2i;
		double f = r2i * r6i * (r6i-0.5);
		Vector2d force = near_vec.array() * f;
		// Total force
		pi.force() -= force;
		pj.force() += force;
	}
}

/* --------- Calculate the force and torque between two rods ---------
 *
 */
void BD_Sim::calc_force_and_torque_2rods_WCA(Particle& pi, Particle& pj){
	// Nearest image convention for CM distance
	Vector2d near_vec = box.nearest_image_vector( pi.pos(), pj.pos() );

	// Early exit if the distance is above the cutoff
	if(near_vec.squaredNorm() > cutoff_sq) return;

	// Calculate respectively the minimum distance between and closest points on the two rods
	double r2, lambda_i, lambda_j;
	pi.shape().approach(
		pi.shape().hl(), pj.shape().hl(),
		pi.rot(), pj.rot(),
		near_vec,
		r2,
		lambda_i, lambda_j
	);

	if(r2 <= 1E-10){ // When the rods overlap or nearly overlap,  we truncate the force
		// std::cout << "Rod overlap WIP. Exiting for now.\n"; exit(42);
		// So we define the force as the CM distance times a (large) number
		Vector2d arm_i = lambda_i * pi.rot();
		Vector2d arm_j = lambda_j * pj.rot();
		Vector2d force = near_vec.array() * max_force;
	 	pi.force() -= force;
	 	pj.force() += force;
		pi.torque() += force[0]*arm_i[1] - force[1]*arm_i[0];
		pj.torque() -= force[0]*arm_j[1] - force[1]*arm_j[0]; // swapped sign of force
	}

	// Calculate forces and torques, only nonzero for r_min/d < 1.0, according to WCA potential
	else if(r2 < 1.2599210498948731648){ // 2^(1/3) = (2^(1/6))^2 = 1.2599210498948731648
		// Minimum distance vector connecting the two lines in the center of the rods (NOT surfaces!)
		Vector2d arm_i = lambda_i * pi.rot();
		Vector2d arm_j = lambda_j * pj.rot();
		Vector2d min_dist_vec = near_vec - arm_i + arm_j;

		// WCA Force calculation
		double r2i = 1.0 / r2;
		double r6i = r2i*r2i*r2i;
		double f = r2i * r6i * (r6i-0.5);
		Vector2d force = min_dist_vec.array() * f;

		// Total force
		pi.force() -= force;
		pj.force() += force;

		// Calculate torque
		pi.torque() += force[0]*arm_i[1] - force[1]*arm_i[0];
		pj.torque() -= force[0]*arm_j[1] - force[1]*arm_j[0]; // swapped sign of force

		// Calculate virial term for pressure calculation
		// virial += force.dot(near_vec);
	}
}

/* --------- Calculate the force and torque between two squares ---------
 *
 */
// void BD_Sim::calc_force_and_torque_2squares_WCA(Particle& pi, Particle& pj){
// 	// Nearest image convention for CM distance
// 	Vector2d near_vec = box.nearest_image_vector( pi.pos(), pj.pos() );
//
// 	// Early exit if the distance is above the cutoff
// 	if(near_vec.squaredNorm() > cutoff_sq) return;
//
// 	// Calculate respectively the minimum distance between and closest points on the two rods
// 	double r2;
// 	Vector2d arm_i, arm_j;
// 	pi.shape().approach(
// 		pj.shape(),
// 		pi.rot(), pj.rot(),
// 		near_vec,
// 		r2,
// 		arm_i, arm_j
// 	);
// 	// std::cout << r2 << '\n';
// 	// std::cout << arm_i << '\n';
// 	// std::cout << arm_j << "\n\n";
//
// 	// NOTE: enabling this does strange things
// 	// if(r2 <= 1E-6){ // When the squares overlap or nearly overlap,  we truncate the force
// 	// 	// We define the force as the CM distance times a (large) number
// 	// 	Vector2d force = near_vec.array() * max_force;
// 	//  	pi.force() -= force;
// 	//  	pj.force() += force;
// 	// 	pi.torque() += force[0]*arm_i[1] - force[1]*arm_i[0]; // T = r x F
// 	// 	pj.torque() -= force[0]*arm_j[1] - force[1]*arm_j[0]; // swapped sign of force
// 	// 	return;
// 	// }
// 	r2 += 0.8 * 0.8;
//
// 	// Calculate forces and torques, only nonzero for r_min/d < 1.0, according to WCA potential
// 	if(r2 < 1.2599210498948731648){ // 2^(1/3) = (2^(1/6))^2 = 1.2599210498948731648
// 		// Minimum distance vector connecting the surfaces of the two squares
// 		Vector2d min_dist_vec = near_vec - arm_i + arm_j;
//
// 		// WCA Force calculation
// 		double r2i = 1.0 / r2;
// 		double r6i = r2i*r2i*r2i;
// 		double f = r2i * r6i * (r6i-0.5);
// 		Vector2d force = min_dist_vec.array() * f;
//
// 		// Total force
// 		pi.force() -= force;
// 		pj.force() += force;
//
// 		// Calculate torque
// 		pi.torque() += force[0]*arm_i[1] - force[1]*arm_i[0];
// 		pj.torque() -= force[0]*arm_j[1] - force[1]*arm_j[0]; // swapped sign of force
//
// 		// Calculate virial term for pressure calculation
// 		// virial += force.dot(near_vec);
// 	}
// }



/* --------- Calculate the forces and torques between all the rods ---------
 *
 */
void BD_Sim::calc_forces(void){
	// virial = 0;

	// Set forces and torques to zero before calculating
	for(Particle& p : particles){
		p.zero_force_and_torque();
	}

	for(Particle& p : particles){
		// for(Particle& pj : particles){
		// 	if( p.id() < pj.id() ){ // Prevents double counting
		// 		// calc_force_and_torque_2squares_WCA(p, pj);
		// 		// calc_force_and_torque_2rods_WCA(p, pj);
		// 		calc_force_2spheres_WCA(p, pj);
		// 	}
		// }

		// std::cout << "Force on particle " << p.id() << '\n' << p.force() << '\n';
		// Get list of neighbour indices from the cell list
		std::vector<size_t> neighbours = cell_list->neighbours_of(p.id());
		for(size_t j = 0; j < neighbours.size(); j++){
      if(p.id() == neighbours[j]){ continue; }
			Particle& pj = particles[ neighbours[j] ];
			// calc_force_and_torque_2squares_WCA(p, pj);
			calc_force_and_torque_2rods_WCA(p, pj);
			// calc_force_2spheres_WCA(p, pj);
		}
	}
}
/* --------- Puts a particle back in the box ---------
 *
 */
bool BD_Sim::put_in_box(Particle& p, Vector2d& to_box){
	Vector2d PBC_vec;
	// Calculate relative position of particle in the simulation volume
	Vector2d Relative_position = box.iL().array() * p.pos().array();

	// If particle is outside of simulation box, add to the translation vector
	for(size_t d = 0; d < 2; d++){
		PBC_vec[d] = -floor(Relative_position[d]);
	}
	// std::cout << "rpos " << Relative_position[0] << " " << Relative_position[1] << "\n";
	// std::cout << "pbcv " << PBC_vec[0] << " " << PBC_vec[1] << "\n";

	// Move the particle back into the box
	to_box = box.L().array() * PBC_vec.array();
	p.translate(to_box);

	// If particle got put back, return true, otherwise false
	if(PBC_vec[0] != 0.0 || PBC_vec[1] != 0.0){
		return true;
	}
	return false;
}
/* --------- Use the Euler-Maruyama scheme to integrate the eqn. of motion ---------
 *
 */
void BD_Sim::integrate_Euler_Maruyama(void){
	bool truncated = false;
	// Define an RNG that follows a Gaussian distribution
	static std::mt19937 GRNG;
  static std::normal_distribution<double> Gaussian(0.0, 1.0);

	// Loop over all particles and integrate both the position and the orientation equations
	for(Particle& p : particles){
		const Vector2d& pos = p.pos();
		const Vector2d& rot = p.rot();
		// ---------- Positions ----------
		p.set_prev_pos(pos);

		// Self-propulsion part
		Vector2d dxA = rot.array() * ic_A;
		// Calculate parallel/perp. friction tensor in sim frame
		Matrix2d uu = rot * rot.transpose();
		Matrix2d I_min_uu = Matrix2d::Identity() - uu;
		Matrix2d friction_m = uu.array() * ic_F_para + I_min_uu.array() * ic_F_perp;
		// Interaction part
		Vector2d dxF = friction_m * p.force();
		// Truncate translation if it is too large
		if(dxF.squaredNorm() > max_translation_sq){
			double rescale_factor = sqrt(max_translation_sq / dxF.squaredNorm());
			dxF *= rescale_factor;
			truncated = true;
		}
		// Brownian part
		Vector2d perp(rot[0], -rot[1]);
		Vector2d dxB = rot.array() * ic_B_para * Gaussian(GRNG) +
                   perp.array() * ic_B_perp * Gaussian(GRNG);
		// Total translation
		Vector2d dx = dxA + dxF + dxB;
	  // Now move the particle and put it back in the box if it moves out
		p.translate( dx );
		Vector2d back_into_box;
		put_in_box(p, back_into_box);

		// ---------- Orientations ----------
		p.set_prev_rot(rot);

		// Torque part
		double daT = p.torque() * ic_T;
		// Truncate torque part if it is too large
		if(fabs(daT) > max_rotation_sq){ daT = copysign(max_rotation_sq,daT); truncated = true; }
		// Brownian part
		double daB = Gaussian(GRNG) * ic_B_rota;
		// Total rotation
		double da = daT + daB;
		// Now rotate the particle by the angle daT+daB
		p.rotate( da );

		// Update the number that tracks whether the forces/torques were too large
		fraction_truncated.add_measurement(truncated);
	}

	// Sort particles back into cells
	cell_list->update(cutoff);
}
/* --------- Use the BAOAB scheme to integrate the eqn. of motion ---------
 * The BAOAB integration scheme is identical to the Euler-Maruyama scheme,
 *  but with the stochastic part G replaced by (G + prev_G) / 2.
 */
void BD_Sim::integrate_BAOAB(void){
	bool truncated = false;
	// Define an RNG that follows a Gaussian distribution
	static std::mt19937 GRNG;
  static std::normal_distribution<double> Gaussian(0.0, 1.0);

	// Make a vector that holds random numbers of the previous step for each particle
	static std::vector<Eigen::Vector3d> prev_GRNG(particles.size(), Eigen::Vector3d::Zero());

	// Loop over all particles and integrate both the position and the orientation equations
	for(Particle& p : particles){
		const Vector2d& pos = p.pos();
		const Vector2d& rot = p.rot();
		// Generate a new array of 3 Gaussian-distributed random numbers
		Eigen::Vector3d new_GRNG( Gaussian(GRNG), Gaussian(GRNG), Gaussian(GRNG) );
		// ---------- Positions ----------
		p.set_prev_pos(pos);

		// Self-propulsion part
		Vector2d dxA = rot.array() * ic_A;
		// Calculate parallel/perp. friction tensor in sim frame
		Matrix2d uu = rot * rot.transpose();
		Matrix2d I_min_uu = Matrix2d::Identity() - uu;
		Matrix2d friction_m = uu.array() * ic_F_para + I_min_uu.array() * ic_F_perp;
		// Interaction part
		Vector2d dxF = friction_m * p.force();
		// Truncate force part if it is too large
		if(dxF.squaredNorm() > max_translation_sq){
			double rescale_factor = sqrt(max_translation_sq / dxF.squaredNorm());
			dxF *= rescale_factor;
			truncated = true;
		}
		// Brownian part
		Vector2d perp(rot[0], -rot[1]);
		Vector2d dxB = rot.array() * ic_B_para * 0.5 * (new_GRNG[0] + prev_GRNG[p.id()][0]) +
		               perp.array() * ic_B_perp * 0.5 * (new_GRNG[1] + prev_GRNG[p.id()][1]);
		// Total translation
		Vector2d dx = dxA + dxF + dxB;

	  // Now move the particle and put it back in the box if it moves out
		p.translate( dx );
		// Vector3d new_nonperiodic_pos = p.pos();
		Vector2d back_into_box;
		put_in_box(p, back_into_box);

		// ---------- Orientations ----------
		p.set_prev_rot(rot);

		// Torque part
		double daT = p.torque() * ic_T;
		// Truncate torque part if it is too large
		if(fabs(daT) > max_rotation_sq){ daT = copysign(max_rotation_sq,daT); truncated = true; }
		// Brownian part
		double daB = 0.5 * (new_GRNG[2] + prev_GRNG[p.id()][2]) * ic_B_rota;
		// Total rotation
		double da = daT + daB;
		// Now rotate the particle by the angle daT+daB
		p.rotate( da );

		// Store random numbers for next step
		prev_GRNG[p.id()] = new_GRNG;
	}

	// Update the number that tracks whether the forces/torques were too large
	fraction_truncated.add_measurement(truncated);

	// Sort particles back into cells
	cell_list->update(cutoff);
}
/* --------- Make a snapshot file of the system ---------
 *
 */
void BD_Sim::save_snapshot(const std::string name){
	// static int nr = 0; // Number of the snapshot
	static bool first = true;

	// Open a file to write to
	std::ofstream snapshot_file;
	// Filename plus number with padded zeroes
	// std::string padded_nr_string = std::string(4-std::to_string(nr).length(),'0')+std::to_string(nr);
	// std::string snapshot_file_name = padded_nr_string + "_" + name + ".dat";
  // snapshot_file.open(snapshot_file_name, std::ios::out | std::ios::trunc);
	// Append to a big coordinate file
	std::string snapshot_file_name = name + ".coord2d";
	if(first){
		snapshot_file.open(snapshot_file_name, std::ios::out | std::ios::trunc);
		first = false;
	}else snapshot_file.open(snapshot_file_name, std::ios::out | std::ios::app);

	// Sort particles into clusters and store the indices for the snapshot
	std::vector<size_t> cluster_idx;
	calc_clusters_density(cluster_idx, max_cluster_fraction, cutoff);
	max_cluster_fraction.save_current_append_to_file("Max_cluster_fraction_time.dat",t);

	// Calculate the local packing fraction by using Voro++
	std::vector<double> voro_cell_areas( particles.size() );
	voronoi_cell_areas_Voro(voro_cell_areas);

	// Save the number of particles
	snapshot_file << '&' << particles.size() << "\n";

	// Save the box. Do some Eigen stuff to ensure correct printing format
	snapshot_file << box.L()[0] << ' ' << box.L()[1] << "\n";

	// Save the particle information
	Vector2d offset = 0.5*box.L(); // Opengl code currently uses (0,0,0) as middle of box, so we offset
	for(Particle& p : particles){
		// Shape index (disk = 0, rod = 1, square = 2)
		snapshot_file << "1 ";
		// Positions
		Vector2d pos = p.pos();
		snapshot_file << pos[0]-offset[0] << ' ' << pos[1]-offset[1] << ' ';
		// Orientations, in axis-angle representation
		Vector2d rot = p.rot();
		snapshot_file << rot[0] << ' ' << rot[1] << ' ';
		// Diameter & length
		snapshot_file << "1 " << p.max_size() << " ";
		// Other numbers (currently respectively dummy, local packing fraction and cluster index)
		// snapshot_file << "0 0 " << cluster_idx[p.id()] << "\n";
		snapshot_file << "0 " << p.area() / voro_cell_areas[p.id()] << ' ' << cluster_idx[p.id()] << "\n";
	}

	// Close file stream and increment number of saved snapshots
	snapshot_file.close();
	// nr++;
}
/* --------- Run a simulation ---------
*
*/
void BD_Sim::run(void){
	bool equilibrated = false;
	double next_save = 0.0;
	// unsigned long int n_measurements = 1E4;
	double measurement_interval = 0.02;
	double next_measurement = 0.0;
	double next_timeseries_save = 0.0, timeseries_interval = 0.1;
	unsigned long int steps_without_truncation = 0;
	std::cout << "Starting Brownian dynamics simulation of rodlike particles.\n";

	for(t = 0.0; t < total_time; t += dt){
		if(!equilibrated && t > parameter_list.init_time){ set_time_step(dt * 0.99); equilibrated = true; }

		// Attempt at a smart adaptive time step
		if(t < parameter_list.init_time){
			if(fraction_truncated.mean() != 0){
				// Shrink timestep if translations/rotations are being truncated
				set_time_step(dt * 0.99);
				fraction_truncated.clear();
				steps_without_truncation = 0;
				// Exit if dt becomes weirdly small to avoid infinite loop
				if(dt < 1E-10){
					std::cout << "time step became improbably small. Exiting.\n";
					exit(42);
				}
			}else if(steps_without_truncation > 10000){
				// Grow timestep if we've gone a while without truncations
				set_time_step(dt * 1.01);
			}else{ steps_without_truncation++; }
		}


		// If we're past the next measurement time, measure
		if( t > next_measurement ){
			// Measure some things all the time
			std::vector<size_t> dummy; calc_clusters_density(dummy, max_cluster_fraction, cutoff);
			// measure some things only after equilibration
			if(equilibrated){
				measure_effective_velocity(effective_velocity);
				measure_orientational_autocorrelation(hist_or_corr, t);
				std::vector<size_t> dummy; calc_clusters_density(dummy, max_cluster_fraction, cutoff);
				measure_local_packfrac_Voro(hist_local_packfrac);
				measure_gr(hist_gr);
			}
			// and schedule next measurement
			next_measurement += measurement_interval;
			// next_measurement += (total_time - parameter_list.init_time) / n_measurements;
		}

		if( t > next_timeseries_save ){
			// Save whatever timeseries we want to measure
			max_cluster_fraction.save_current_append_to_file("Max_cluster_fraction_time.dat",t);
			next_timeseries_save += timeseries_interval;
		}


		// Integrate forward one time step
		calc_forces();
		// integrate_Euler_Maruyama();
		integrate_BAOAB();


		// Save to a file occasionally
		if( t >= next_save ){
			next_save += total_time / n_snapshots;
			// std::cout << "Saved @ " << t << '\n';
			save_snapshot("Coords");
			if(fraction_truncated.mean() == 0){
				std::cout << "Saved @ t = " << t << ", dt = " << dt << '\n';
			}else{
				std::cout << "Saved @ t = " << t << ", dt = " << dt << ", truncated = " << fraction_truncated.mean() << '\n';
			}
			fraction_truncated.clear();
			// save measured data
			if(equilibrated){
				hist_or_corr.save_to_file("Orientational_autocorrelation.dat");
				effective_velocity.save_mean_to_file("Effective_velocity.dat");
				hist_local_packfrac.save_to_file("local_packfrac.dat");
			}
			hist_gr.save_to_file("gr.dat");
			max_cluster_fraction.save_mean_to_file("Max_cluster_fraction.dat");
		}
	}

	// save measured data before exiting
	save_snapshot("Coords");
	hist_or_corr.save_to_file("Orientational_autocorrelation.dat");
	effective_velocity.save_mean_to_file("Effective_velocity.dat");
	hist_gr.save_to_file("gr.dat");
	max_cluster_fraction.save_mean_to_file("Max_cluster_fraction.dat");
	max_cluster_fraction.save_current_append_to_file("Max_cluster_fraction_time.dat",t);
	hist_local_packfrac.save_to_file("local_packfrac.dat");
}



/* --------- Sort all particles into clusters by proximity ---------
 * The criterion for clustering is that two particles are separated
 * by less than the cutoff distance.
 */
void BD_Sim::calc_clusters(
	std::vector<size_t>& p_cluster_idx,
	statistics::Observable& max_cluster_frac,
	double cutoff
){
	// The cluster cutoff is the interaction range of the WCA potential
	double cluster_cutoff = pow(2.0, 1.0/6.0);
	double cluster_cutoff_sq = cluster_cutoff * cluster_cutoff;

	// Clear the old cluster index list if there are any
	p_cluster_idx.clear();
	// Also make a list that stores particle indices per cluster
	std::vector<std::vector<size_t>> clusters;
	// Start with each particle being its own cluster
	for(size_t i = 0; i < particles.size(); i++){
		p_cluster_idx.push_back(i);
	}

	// Loop over particle pairs and make clusters (similar to Wolff algorithm)
	std::stack<size_t> cluster_stack;
	for(Particle& p : particles){
		size_t cluster_idx;
		// If particle p is alone in its cluster, build its cluster
		if( p_cluster_idx[p.id()] == p.id() ){
			cluster_idx = clusters.size();
			clusters.push_back(std::vector<size_t>(1,p.id()));
			p_cluster_idx[p.id()] = cluster_idx;
			// Put p onto a stack for particles in the cluster
			cluster_stack.push(p.id());
			// While any particle in the cluster has neighbours, check them for adding to cluster
			while( !cluster_stack.empty() ){
				// Grab a particle from the stack to check its neighbours
				Particle& p = particles[ cluster_stack.top() ];
				cluster_stack.pop();
				std::vector<size_t> p_neighbours = cell_list->neighbours_of(p.id());
				for(size_t nb_idx : p_neighbours){
					Particle& pj = particles[nb_idx];
					// If this pj is not already in the cluster
					if( p_cluster_idx[pj.id()] != cluster_idx ){
						// Calculate based on distance whether to add pj to cluster
						Vector2d near_vec = box.nearest_image_vector( p.pos(), pj.pos() );
						double r2, lambda_i, lambda_j;
						p.shape().approach(
							p.shape().hl(), pj.shape().hl(),
							p.rot(), pj.rot(),
							near_vec,
							r2,
							lambda_i, lambda_j
						);
						// If within cutoff
						if( r2 < cluster_cutoff_sq ){
							// Add pj to cluster and set pj's cluster idx to the right number
							clusters[cluster_idx].push_back(pj.id());
							p_cluster_idx[pj.id()] = cluster_idx;
							// Also add to the stack so pj's neighbours get checked too
							cluster_stack.push(nb_idx);
						}
					}
				}
			}
		}
	}

	// Calculate the fraction of particles that are part of the largest cluster
	double max_frac = 0.0;
	for(auto& cluster : clusters){
		double frac = cluster.size() / ((double) particles.size());
		if(frac > max_frac){ max_frac = frac; }
	}
	max_cluster_frac.add_measurement(max_frac);
}
/* --------- Sort all particles into clusters by proximity and density ---------
 * The criterion for clustering is that two particles that are neighbours
 * by Voronoi tesselation are similar in local packing fraction.
 */
void BD_Sim::calc_clusters_density(
	std::vector<size_t>& p_cluster_idx,
	statistics::Observable& max_cluster_frac,
	double cutoff
){
	// The cluster cutoff is the interaction range of the WCA potential
	// double cluster_cutoff = pow(2.0, 1.0/6.0);
	// double cluster_cutoff_sq = cluster_cutoff * cluster_cutoff;
	// double packfrac_cutoff = 0.04;
	double global_packfrac = particles[0].area() * particles.size() / box.area();
	// How much local packing fraction must be above / below mean to form a bond
	double offset = 0.025;

	// Clear the old cluster index list if there are any
	p_cluster_idx.clear();
	// Also make a list that stores particle indices per cluster
	std::vector<std::vector<size_t>> clusters;
	// Start with each particle being its own cluster
	for(size_t i = 0; i < particles.size(); i++){
		p_cluster_idx.push_back(i);
	}

	// add particles into the Voro++ container
	voro_box->clear();
	for(Particle& p : particles){
		voro_box->put(p.id(), p.pos()[0], p.pos()[1], 0);
	}
	// Calculate the Voronoi tesselation and the neighbours of every cell / particle
	voro::c_loop_all cla(*voro_box);
	voro::voronoicell_neighbor c;
	// NOTE: voro negative index means neighbour is its own periodic image
	std::vector<std::vector<int>> p_neighbours(particles.size());
	std::vector<double> cell_areas(particles.size());
	if( cla.start() ) do if( voro_box->compute_cell(c,cla) ){
		c.neighbors( p_neighbours[cla.pid()] );
		cell_areas[cla.pid()] = c.volume(); // height is 1 => volume is area
	} while(cla.inc());

	// Loop over particle pairs and make clusters (similar to Wolff algorithm)
	std::stack<size_t> cluster_stack;
	for(Particle& p : particles){
		size_t cluster_idx;
		// If particle p is alone in its cluster, build its cluster
		if( p_cluster_idx[p.id()] == p.id() ){
			cluster_idx = clusters.size();
			clusters.push_back(std::vector<size_t>(1,p.id()));
			p_cluster_idx[p.id()] = cluster_idx;
			// Put p onto a stack for particles in the cluster
			cluster_stack.push(p.id());
			// While any particle in the cluster has neighbours, check them for adding to cluster
			while( !cluster_stack.empty() ){
				// Grab a particle from the stack to check its neighbours
				Particle& p = particles[ cluster_stack.top() ];
				double packfrac_p = p.area() / cell_areas[p.id()];
				cluster_stack.pop();
				// Get particle p's neighbours from its Voronoi cell's neighbours
				for(size_t nb_idx : p_neighbours[p.id()]){
					Particle& pj = particles[nb_idx];
					// If this pj is not already in the cluster
					if( p_cluster_idx[pj.id()] != cluster_idx ){
						double packfrac_pj = pj.area() / cell_areas[pj.id()];
						// If density is similar and distance is not large, add pj to cluster
						// Vector2d near_vec = box.nearest_image_vector( p.pos(), pj.pos() );
						// double r2, lambda_i, lambda_j;
						// p.shape().approach(
						// 	p.shape().hl(), pj.shape().hl(),
						// 	p.rot(), pj.rot(),
						// 	near_vec,
						// 	r2,
						// 	lambda_i, lambda_j
						// );
						// if( fabs(packfrac_pj - packfrac_p) < packfrac_cutoff /*&& r2 < cluster_cutoff_sq*/){
						// 	// Add pj to cluster and set pj's cluster idx to the right number
						// 	clusters[cluster_idx].push_back(pj.id());
						// 	p_cluster_idx[pj.id()] = cluster_idx;
						// 	// Also add to the stack so pj's neighbours get checked too
						// 	cluster_stack.push(nb_idx);
						// }
						// If density of both particles is similar and far enough away from global mean
						if(
							(packfrac_p > global_packfrac + offset && packfrac_pj > global_packfrac + offset) ||
							(packfrac_p < global_packfrac - offset && packfrac_pj < global_packfrac - offset)
						){
							// Add pj to cluster and set pj's cluster idx to the right number
							clusters[cluster_idx].push_back(pj.id());
							p_cluster_idx[pj.id()] = cluster_idx;
							// Also add to the stack so pj's neighbours get checked too
							cluster_stack.push(nb_idx);
						}
					}
				}
			}
		}
	}

	// Calculate the fraction of particles that are part of the largest cluster
	double max_frac = 0.0;
	for(auto& cluster : clusters){
		double frac = cluster.size() / ((double) particles.size());
		if(frac > max_frac){ max_frac = frac; }
	}
	max_cluster_frac.add_measurement(max_frac);
}
/* --------- Measures the radial distribution function ---------
 * Calculates the two-body radial distribution function. First calculates what it would be for
 * an ideal gas, then measures for the current positions of all particles in the simulation.
 */
void BD_Sim::measure_gr(statistics::Histogram& hist){
	static bool first = true;
	static double max_dist = 10.0;
	static std::vector<double> gr_idgas;
	const double binwidth = 0.01;
	// On first call, initialize histogram and calculate the g(r) of an ideal gas
	if(first){
		// max_dist = (box.hL()[0] < box.hL()[1]) ? box.hL()[0] : box.hL()[1];
		// Initialize the g(r) histogram to run from 0 to maxdist
		hist.initialize_with_size(binwidth, 0.0, max_dist);
		// Calculate g(r) of ideal gas
		gr_idgas.resize( hist.size() );
		double density = ((double) particles.size()) / box.area();
		for(size_t i = 0; i < gr_idgas.size(); i++){
			gr_idgas[i] = M_PI * density * (pow((i+1)*binwidth,2) - pow(i*binwidth,2));
		}
		first = false;
	}

	// Gather counts for the g(r) histogram
	double dist, one_over_n = 1.0 / ((double) particles.size());
	std::vector<size_t> counts;
	for(size_t i = 0; i < hist.size(); i++){ counts.push_back(0); }
	// Gather counts by full O(N^2) loop
	// for(size_t i = 0; i < particles.size()-1; i++){
	// 	Particle& pi = particles[i];
	// 	for(size_t j = i+1; j < particles.size(); j++){
	// 		Particle& pj = particles[j];
	// 		dist = box.nearest_image_distance(pi.pos(), pj.pos());
	// 		size_t bin = floor(dist / binwidth);
	// 		if( bin < counts.size() ){ // dist can exceed max_dist, so add only if in range
	// 			counts[bin] += 2; // pair distance, so 2 counts
	// 		}
	// 	}
	// }
	for(Particle& pi : particles){
		std::vector<size_t> neighbours = cell_list->neighbours_of(pi.id());
		for(size_t j = 0; j < particles.size(); j++){
			Particle& pj = particles[j];
			if(j >= pi.id()){continue;} // prevent double counting calculations
			dist = box.nearest_image_distance(pi.pos(), pj.pos());
			size_t bin = floor(dist / binwidth);
			if( bin < counts.size() ){ // dist can exceed max_dist, so add only if in range
				counts[bin] += 2; // pair distance, so 2 counts
			}
		}
	}

	// Calculate the actual g(r) values of the histogram
	for(size_t bin = 0; bin < hist.size(); bin++){
		hist.add_measurement(bin, one_over_n * counts[bin] / gr_idgas[bin]);
	}
}
/* --------- Builds a histogram of how the particle orientations decay in time ---------
 *
 */
void BD_Sim::measure_orientational_autocorrelation(statistics::DynamicHistogram& hist, double t){
	static int calls = 0;
	// Vector of vectors storing all particle orientations at all measured times
	static std::vector<std::vector<Vector2d>> stored_orientations( particles.size() );
	// Vector storing all times at which orientations were stored
	static std::vector<double> stored_times;
	const size_t max_hist_hist = 1E4;

	// To start measuring, we need a binwidth and two data points
	if(calls == 0){
		// Binwidth unknown, just store the first orientations and reserve memory for now
		stored_times.reserve(max_hist_hist);
		stored_orientations.reserve(max_hist_hist);
		hist.reserve(max_hist_hist);
		stored_times.push_back(t);
		for(Particle& p : particles){
			stored_orientations[p.id()].push_back( p.rot() );
		}
		calls++;
		return;
	}else if(calls == 1){
		// Initialize the histogram (1 bin, binwidth=t-stored[0], starts at time delay 0.0)
		hist.initialize(1, t - stored_times[0], 0.0);
		calls++;
	}

	// Make sure the histogram can hold the new largest time delay
	if(hist.size() < max_hist_hist){
		hist.grow_to_include(t - stored_times[0]);
		// Store the latest measurement time
		stored_times.push_back(t);
		// Store the latest time orientations
		for(Particle& p : particles){
			stored_orientations[p.id()].push_back( p.rot() );
		}
	}else{
		// Once we stop growing histogram, start moving the first measured time
		for(size_t i = 0; i < stored_times.size(); i++){
			stored_times[i] = stored_times[i+1];
			for(size_t pidx = 0; pidx < particles.size(); pidx++){
				stored_orientations[pidx][i] = stored_orientations[pidx][i+1];
			}
		}
		stored_times.back() = t;
		for(Particle& p : particles){
			stored_orientations[p.id()].back() = p.rot();
		}
	}

	// Now calculate the autocorrelations at all time delays
	for(Particle& p : particles){
		for(size_t i = 0; i < stored_times.size(); i++){
			// Calculate the autocorrelation at this time delay
			double dt = t - stored_times[i];
			double auto_correlation = p.rot().dot( stored_orientations[p.id()][i] );
			hist.add_measurement(dt, auto_correlation);
		}
	}
}
/* --------- Measure the mean effective self-propulsion velocity ---------
 * The effective self-propulsion velocity is given by the equal-time velocity-orientation
 * correlation function. Since we measure the velocity from the difference in position
 * between two time steps, we need to average the orientation of two time steps to evaluate
 * the orientation at the same time as the velocity.
 */
void BD_Sim::measure_effective_velocity(statistics::Observable& veff){
	double one_over_dt = 1.0 / dt;
	for(Particle& p : particles){
		Vector2d velocity = box.nearest_image_vector(p.prev_pos(), p.pos()).array() * one_over_dt;
		// Mind that velocity is measured in the middle of the time step,
		// so we should also measure the orientation at that time.
		Vector2d rot_at_middle_step = (p.rot() + p.prev_rot()).array() * 0.5;
		veff.add_measurement( velocity.dot(rot_at_middle_step) );
	}
}
/* --------- Use Voro++ to calculate local density ---------
 * Voro++ is a library that can do Voronoi tesselations. We let it calculate the
 * volume of each particle's Voronoi cell.
 */
void BD_Sim::voronoi_cell_areas_Voro(std::vector<double>& cell_areas){
	// add particles into the Voro++ container
	voro_box->clear();
	for(size_t i = 0; i < particles.size(); i++){
		voro_box->put(i, particles[i].pos()[0], particles[i].pos()[1], 0);
	}

	// Use the voro++ custom loops to loop over all cells and get their volumes
	voro::c_loop_all cla(*voro_box);
	voro::voronoicell c;
	if( cla.start() ) do if( voro_box->compute_cell(c,cla) ){
		// cla.pid() gives the index of the cell / particle currently being
		// considered. c.volume() then gives its Voronoi cell's volume.
		// Since height is 1, volume is area.
		cell_areas[cla.pid()] = c.volume();
	} while(cla.inc());
}
/* --------- Use Voro++ to calculate local density ---------
 * Voro++ is a library that can do Voronoi tesselations. We let it calculate the
 * volume of each particle's Voronoi cell and use that to calculate the local
 * density / packing fraction.
 */
void BD_Sim::measure_local_packfrac_Voro(statistics::Histogram& hist){
	static bool first = true;
	const double binwidth = 0.005;
	// On first call, initialize histogram
	if(first){
		// Initialize the histogram to run from 0 to 1 in steps of binwidth
		hist.initialize_with_size(binwidth, 0.0, 1.0);
		first = false;
	}

	// Call on Voro for the Voronoi cell areas
	std::vector<double> cell_areas( particles.size() );
	voronoi_cell_areas_Voro(cell_areas);

	// Gather counts
	std::vector<unsigned long int> counts( hist.size() );
	for(size_t i = 0; i < cell_areas.size(); i++){
		size_t bin = floor(particles[i].area() / cell_areas[i] * hist.size());
		if( bin < hist.size() ){ counts[bin]++; }
		else{ counts[hist.size()-1]++; } // add to last bin if outside of range
	}

	// Update histogram
	for(size_t i = 0; i < hist.size(); i++){
		hist.add_measurement(i, ((double)counts[i] / (double)particles.size()) );
	}
}
