#include <random>

class tRnd {
  private :
    std::random_device                            rd;
    std::mt19937                                  mt;
    std::uniform_real_distribution<double>        distd;
    std::uniform_int_distribution<int>            disti;
    std::uniform_int_distribution<unsigned int>   distui;
  public:
    tRnd() : rd{}, mt{rd()} {}
		// Seed the pseudo-RNG with a single integer number
		void seed(int seed_nr){
			mt.seed(seed_nr);
		}
		// Seeds the pseudo-RNG from the number of clock cycles since the processor was turned on.
		// This should effectively generate a unique seed if you start many RNG's in parallel on
		// different processors, but doesn't guarantee uniqueness. Since this looks like a crazy
		// bit hack, here's the source:
		// https://stackoverflow.com/questions/7617587/is-there-an-alternative-to-using-time-to-seed-a-random-number-generation
		void seed_unique(void){
	    unsigned int lo, hi;
	    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
			mt.seed( ((unsigned long long)hi << 32) | lo );
		}

		// Random doubles
    inline double rand(){
      distd=std::uniform_real_distribution<double>(0,1);
      return distd(mt);
    }
    inline double rand(const double b){
      distd=std::uniform_real_distribution<double>(0,b);
      return distd(mt);
    }
    inline double rand(const double a, const double b){
      distd=std::uniform_real_distribution<double>(a,b);
      return distd(mt);
    }

		// Random integers
    inline int rani(const int b){
      disti=std::uniform_int_distribution<int>(0,b);
      return disti(mt);
    }
    inline int rani(const int a, const int b){
      disti=std::uniform_int_distribution<int>(a,b);
      return disti(mt);
    }
		inline int rani(const unsigned int b){
      distui=std::uniform_int_distribution<unsigned int>(0,b);
      return distui(mt);
    }
    inline int rani(const unsigned int a, const unsigned int b){
      distui=std::uniform_int_distribution<unsigned int>(a,b);
      return distui(mt);
    }
		inline int rani(const int a, const unsigned int b){
      distui=std::uniform_int_distribution<unsigned int>(a,b);
      return distui(mt);
    }
		inline int rani(const unsigned int a, const int b){
      distui=std::uniform_int_distribution<unsigned int>(a,b);
      return distui(mt);
    }
		// Random vectors
    inline Eigen::Vector3d randVec3d(void){
      double x,y,z,l2;
      do{
        x = rand(-1,+1);
        y = rand(-1,+1);
        z = rand(-1,+1);
        l2 = x*x + y*y + z*z;
      } while (l2 > 1 || l2 < 0.0000001);
      return Eigen::Vector3d(x,y,z);
    }
		inline Eigen::Vector3d randUnitVec3d(void){
			double x,y,z,l2;
      do{
        x = rand(-1,+1);
        y = rand(-1,+1);
        z = rand(-1,+1);
        l2 = x*x + y*y + z*z;
      } while (l2 > 1.0 || l2 < 0.0000001);
			l2 = sqrt(l2);
      return Eigen::Vector3d(x/l2,y/l2,z/l2);
    }
		// Random quaternions
    inline Eigen::Quaterniond randomUnitQuaternion(void){
      const double u1 = rand(0, 1),
                   u2 = rand(0, 2*M_PI),
                   u3 = rand(0, 2*M_PI);
      const double a = sqrt(1 - u1),
                   b = sqrt(u1);
      return Eigen::Quaterniond (a * sin(u2), a * cos(u2), b * sin(u3), b * cos(u3));
    }
    inline Eigen::Quaterniond randomUnitQuaternion(double dalpha){
      Eigen::Vector3d v=randUnitVec3d();
      double alpha=rand(dalpha);
      v=v*sin(2*alpha);
      return Eigen::Quaterniond(cos(2*alpha),v[0],v[1],v[2]);
    }
};
