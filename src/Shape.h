#ifndef BD_SHAPE_H
#define BD_SHAPE_H

#include <eigen3/Eigen/Geometry>
#include <memory>
#include <iostream>

using Eigen::Vector2d;
using Eigen::Matrix2d;
using Eigen::Vector2i;

namespace Shape {

  // Function that gives closest distance between and closest points of two line segments
  void line_line_distance(
    const double hl_1, const double hl_2,
    const Vector2d& rot_1, const Vector2d& rot_2,
    const Vector2d& cm_vec,
    double& dist_sq,
    double& la_1, double& la_2
  );

  // ==========================================================================
  // ================================ SPHERE ==================================
  // ==========================================================================
  class Sphere {
  private:
    double area_;
    double max_size_;

    double diameter_;
  public:
    Sphere(void) = delete;
    Sphere(double diameter) :
      diameter_(diameter)
    {
      area_ = M_PI * (diameter / 2.0) * (diameter / 2.0);
      max_size_ = diameter;
    }
    virtual ~Sphere(){};

    // Area occupied by particle
    double area(void)const{return area_;}
    // Maximum size of shape. Essentially radius of circumscribed sphere.
    double max_size(void)const{return max_size_;}
    // Size getter
    double diameter(void)const{return diameter_;}

    // Comparison operators
    friend bool operator==(const Sphere& lhs, const Sphere& rhs){
			return (lhs.diameter_ == rhs.diameter_);
		};
		friend bool operator!=(const Sphere& lhs, const Sphere& rhs){
			return !(lhs == rhs);
		};
  };



  // ==========================================================================
  // ================================== ROD ===================================
  // ==========================================================================
  class Rod {
  private:
    double area_;
    double max_size_;

    double width_;
    double length_;
    double aspect_ratio_;

    double hl_; // length/2 of cylinder part, stored separately for speed
  public:
    Rod(void) = delete;
    Rod(double width, double length) :
      width_(width), length_(length)
    {
      if(length < width){
        std::cout << "Error: rod shape with aspect ratio < 1 requested.\n";
        exit(42);
      }
      aspect_ratio_ = length / width;
      area_ = M_PI * (width / 2.0) * (width / 2.0) + width * (aspect_ratio_ - 1.0);
      max_size_ = length;
      hl_ = (length - width) / 2.0;
    }
    virtual ~Rod(){};

    // Area occupied by particle
    double area(void)const{return area_;}
    // Maximum size of shape. Essentially radius of circumscribed sphere.
    double max_size(void)const{return max_size_;}
    // Size getters
    double width(void)const{return width_;}
    double length(void)const{return length_;}
    double aspect_ratio(void)const{return aspect_ratio_;}
    double hl(void)const{return hl_;}

    // Shortest distance between two rods is same as between two line segments
    void approach(
      const double hl_1, const double hl_2,
      const Vector2d& rot_1, const Vector2d& rot_2,
      const Vector2d& cm_vec,
      double& dist_sq,
      double& la_1, double& la_2
    )const;

    // Comparison operators
    friend bool operator==(const Rod& lhs, const Rod& rhs){
      return (
        lhs.width_  == rhs.width_ &&
        lhs.length_ == rhs.length_
      );
    };
    friend bool operator!=(const Rod& lhs, const Rod& rhs){
      return !(lhs == rhs);
    };
  };

  // ==========================================================================
  // ================================ SQUARE ==================================
  // ==========================================================================
  class Square {
  private:
    double area_;
    double max_size_;

    double edge_length_;
    double hl_;
    std::array<Vector2d,4> vertex_;
  public:
    Square(void) = delete;
    Square(double edge_length) :
      edge_length_(edge_length), hl_(edge_length/2.0),
      vertex_({
        Vector2d(-edge_length/2.0,-edge_length/2.0),
        Vector2d( edge_length/2.0,-edge_length/2.0),
        Vector2d( edge_length/2.0, edge_length/2.0),
        Vector2d(-edge_length/2.0, edge_length/2.0)
      })
    {
      if(edge_length < 0){
        std::cout << "Error: square shape requested with negative size. Exiting.\n";
        exit(42);
      }
      area_ = edge_length_ * edge_length_;
      max_size_ = sqrt(2.0) * edge_length_;
    }
    virtual ~Square(){};

    // Area occupied by particle
    double area(void)const{return area_;}
    // Maximum size of shape. Essentially radius of circumscribed sphere.
    double max_size(void)const{return max_size_;}
    // Size getters
    double edge_length(void)const{return edge_length_;}
    double hl(void)const{return hl_;}

    // Comparison operators
    friend bool operator==(const Square& lhs, const Square& rhs){
      return (lhs.edge_length_ == rhs.edge_length_);
    };
    friend bool operator!=(const Square& lhs, const Square& rhs){
      return !(lhs == rhs);
    };

    // Function that gives closest distance between and closest points of two squares
    void approach(
      const Square& other,
      const Vector2d& rot_1, const Vector2d& rot_2,
      const Vector2d& cm_vec,
      double& dist_sq,
      Vector2d& arm_1, Vector2d& arm_2
    )const;


  };
}

#endif
